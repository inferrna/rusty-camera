// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![allow(non_upper_case_globals, dead_code, non_camel_case_types, improper_ctypes, non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/android_c_headers.rs"));
