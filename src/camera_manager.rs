// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

use crate::android_impl::{
    WACameraCaptureSession, WACameraDevice, WACameraManager, WACameraOutputTarget,
    WACaptureRequest, WACaptureSessionOutput, WACaptureSessionOutputContainer, WANativeWindow,
};
use crate::camera_listeners::{
    GetCaptureCallback, OnCameraAvailable, OnCameraUnavailable, OnDeviceErrorChanges,
    OnDeviceStateChanges, OnSessionActive, OnSessionClosed, OnSessionReady,
};
use crate::camera_manager::CaptureSessionState::{ACTIVE, READY};
use crate::camera_manager::PREVIEW_INDICES::{
    JPG_CAPTURE_REQUEST_IDX, PREVIEW_REQUEST_IDX, RAW_CAPTURE_REQUEST_IDX,
};
use crate::AndroidUnited::acamera_metadata_enum_acamera_control_ae_mode::{
    ACAMERA_CONTROL_AE_MODE_OFF, ACAMERA_CONTROL_AE_MODE_ON,
};
use crate::AndroidUnited::acamera_metadata_enum_acamera_control_af_mode::ACAMERA_CONTROL_AF_MODE_AUTO;
use crate::AndroidUnited::acamera_metadata_enum_acamera_lens_facing::ACAMERA_LENS_FACING_EXTERNAL;
use crate::AndroidUnited::acamera_metadata_tag::{
    ACAMERA_CONTROL_AE_MODE, ACAMERA_CONTROL_AF_MODE, ACAMERA_JPEG_ORIENTATION,
    ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, ACAMERA_SENSOR_EXPOSURE_TIME,
    ACAMERA_SENSOR_INFO_EXPOSURE_TIME_RANGE, ACAMERA_SENSOR_INFO_SENSITIVITY_RANGE,
    ACAMERA_SENSOR_ORIENTATION, ACAMERA_SENSOR_SENSITIVITY,
};
use crate::AndroidUnited::ACameraDevice_request_template::{
    TEMPLATE_PREVIEW, TEMPLATE_STILL_CAPTURE,
};
use crate::AndroidUnited::AIMAGE_FORMATS::{AIMAGE_FORMAT_JPEG, AIMAGE_FORMAT_YUV_420_888};
use crate::AndroidUnited::{
    acamera_metadata_tag, acamera_type, ACameraCaptureSession_capture,
    ACameraCaptureSession_setRepeatingRequest, ACameraCaptureSession_stopRepeating,
    ACameraDevice_StateCallbacks, ACameraDevice_close, ACameraDevice_createCaptureRequest,
    ACameraDevice_createCaptureSession, ACameraDevice_getId, ACameraManager_openCamera,
    ACameraManager_registerAvailabilityCallback, ACameraOutputTarget_create,
    ACaptureRequest_addTarget, ACaptureRequest_setEntry_i32, ACaptureRequest_setEntry_i64,
    ACaptureRequest_setEntry_u8, ACaptureSessionOutputContainer_add,
    ACaptureSessionOutputContainer_create, ACaptureSessionOutput_create, ANativeWindow_acquire,
    CamErrType, AIMAGE_FORMATS,
};
use crate::{
    camera_manager::CaptureSessionState::MAX_STATE,
    camera_utils::GetTagStr,
    AndroidUnited::{
        acamera_metadata_enum_acamera_lens_facing,
        acamera_metadata_enum_acamera_lens_facing::{
            ACAMERA_LENS_FACING_BACK, ACAMERA_LENS_FACING_FRONT,
        },
        acamera_metadata_tag::ACAMERA_LENS_FACING,
        ACameraCaptureFailure, ACameraCaptureSession, ACameraCaptureSession_captureCallbacks,
        ACameraCaptureSession_stateCallbacks, ACameraDevice, ACameraDevice_request_template,
        ACameraDevice_stateCallbacks, ACameraIdList, ACameraManager,
        ACameraManager_AvailabilityCallbacks, ACameraManager_create,
        ACameraManager_getCameraCharacteristics, ACameraManager_getCameraIdList, ACameraMetadata,
        ACameraMetadata_const_entry, ACameraMetadata_free, ACameraMetadata_getAllTags,
        ACameraMetadata_getConstEntry, ACameraOutputTarget, ACaptureRequest, ACaptureSessionOutput,
        ACaptureSessionOutputContainer, ANativeWindow,
    },
};
use core::{ops, time};
use num::Integer;
use num_traits::{FromPrimitive, PrimInt};
use std::borrow::{Borrow, BorrowMut};
use std::cell::RefCell;
use std::cmp::Ordering;
use std::ffi::{c_void, CStr, CString};
use std::marker::{PhantomData, PhantomPinned};
use std::ops::Deref;
use std::os::raw::c_char;
use std::pin::Pin;
use std::ptr::{null, null_mut};
use std::rc::Rc;
use std::sync::atomic::AtomicPtr;
use std::thread::sleep;
use std::{collections::HashMap, mem::MaybeUninit, ops::Range, os::raw::c_uchar};

type int32_t = i32;

static kMinExposureTime: u64 = 1000000;
static kMaxExposureTime: u64 = 250000000;

#[macro_export]
macro_rules! get_new_entry {
    ( $m:expr, $t:expr ) => {{
        debug!("Get entry for tag {:?} from {} at {}", $t, file!(), line!());
        let mut _lensInfo = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
        let status = unsafe_no_check!(ACameraMetadata_getConstEntry(
            $m,
            $t as u32,
            _lensInfo.as_mut_ptr()
        ));
        match status {
            ACAMERA_OK => Some(unsafe_no_check!(_lensInfo.assume_init())),
            _ => {
                debug!("Got status {:?} for tag {:?}", status, $t);
                None
            }
        }
    }};
}

macro_rules! get_new_metadata {
    ( $mgr:expr, $camid:expr ) => {{
        debug!("Call to get metadata from {} at {}", file!(), line!());
        let mut _metadata = MaybeUninit::<*mut ACameraMetadata>::uninit();
        unsafe_and_check_camera!(ACameraManager_getCameraCharacteristics(
            $mgr,
            $camid,
            _metadata.as_mut_ptr()
        ));
        unsafe { _metadata.assume_init() }
    }};
}
macro_rules! c_str_from_string {
    ( $str:expr ) => {{
        let c_str = CString::new($str).unwrap();
        c_str.as_ptr()
    }};
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub(crate) enum CaptureSessionState {
    READY = 0isize,
    ACTIVE = 1isize,
    CLOSED = 2isize,
    MAX_STATE = 3isize,
}

#[derive(Debug, Copy, Clone)]
pub(crate) struct ImageFormat {
    pub(crate) width: i32,
    pub(crate) height: i32,
    pub(crate) format: AIMAGE_FORMATS,
}

struct RangeValue<T> {
    pub min_: T,
    pub max_: T,
}

trait MyRanged {}
impl MyRanged for i32 {}
impl MyRanged for u32 {}

impl<T> RangeValue<T>
where
    T: Integer + PrimInt + From<u32> + FromPrimitive,
{
    fn new() -> RangeValue<T> {
        RangeValue {
            min_: 0u32.into(),
            max_: 0u32.into(),
        }
    }
    fn value(&self, percent: u32) -> T {
        (self.min_ + (self.max_ - self.min_) * (percent.into()) / (100u32.into()))
    }
    fn Supported(&self) -> bool {
        self.min_ != self.max_
    }
}

#[derive(Debug, FromPrimitive, Clone, Copy, PartialEq, Eq, Hash)]
pub(crate) enum PREVIEW_INDICES {
    PREVIEW_REQUEST_IDX = 0isize,
    JPG_CAPTURE_REQUEST_IDX,
    RAW_CAPTURE_REQUEST_IDX,
    CAPTURE_REQUEST_COUNT,
}

struct CaptureRequestInfo {
    outputNativeWindow_: WANativeWindow,
    sessionOutput_: WACaptureSessionOutput,
    target_: WACameraOutputTarget,
    request_: WACaptureRequest,
    template_: ACameraDevice_request_template,
    sessionSequenceId_: i32,
}

#[derive(Debug, Copy, Clone)]
pub(crate) struct DisplayDimension {
    w_: i32,
    h_: i32,
    portrait_: bool,
}

impl DisplayDimension {
    pub(crate) fn new(w: i32, h: i32) -> DisplayDimension {
        DisplayDimension {
            w_: w,
            h_: h,
            portrait_: true,
        }
    }
    pub(crate) fn IsSameRatio(&self, other: &Self) -> bool {
        self.org_width() * other.org_height() == self.org_height() * other.org_width()
    }
    pub(crate) fn Flip(&mut self) {
        self.portrait_ = !self.portrait_
    }
    pub(crate) fn IsPortrait(&mut self) -> bool {
        self.portrait_
    }
    pub(crate) fn width(&self) -> i32 {
        self.w_
    }
    pub(crate) fn height(&self) -> i32 {
        self.h_
    }
    pub(crate) fn org_width(&self) -> i32 {
        match self.portrait_ {
            true => self.h_,
            false => self.w_,
        }
    }
    pub(crate) fn org_height(&self) -> i32 {
        match self.portrait_ {
            true => self.w_,
            false => self.h_,
        }
    }
}

impl From<Option<&DisplayDimension>> for DisplayDimension {
    fn from(other: Option<&Self>) -> Self {
        match other {
            Some(x) => DisplayDimension {
                w_: x.w_,
                h_: x.h_,
                portrait_: x.portrait_,
            },
            None => DisplayDimension {
                w_: 0,
                h_: 0,
                portrait_: false,
            },
        }
    }
}

impl ops::Sub<DisplayDimension> for DisplayDimension {
    type Output = DisplayDimension;
    fn sub(self, other: Self) -> DisplayDimension {
        DisplayDimension::new(self.w_ - other.w_, self.h_ - other.h_)
    }
}

impl PartialEq for DisplayDimension {
    fn eq(&self, other: &Self) -> bool {
        self.org_width() == other.org_width() && self.org_height() == other.org_height()
        // && self.portrait_ == other.portrait_
    }

    fn ne(&self, other: &Self) -> bool {
        self.org_width() != other.org_width() || self.org_height() != other.org_height()
        // self.portrait_ != other.portrait_
    }
}

impl PartialOrd for DisplayDimension {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let res = other.w_.partial_cmp(&self.w_) == other.h_.partial_cmp(&self.h_);
        match res {
            true => other.w_.partial_cmp(&self.w_),
            false => None,
        }
    }
    #[inline]
    fn lt(&self, other: &Self) -> bool {
        self.w_ < other.w_ && self.h_ < other.h_
    }
    #[inline]
    fn le(&self, other: &Self) -> bool {
        self.w_ <= other.w_ && self.h_ <= other.h_
    }
    #[inline]
    fn gt(&self, other: &Self) -> bool {
        self.w_ > other.w_ && self.h_ > other.h_
    }
    #[inline]
    fn ge(&self, other: &Self) -> bool {
        self.w_ >= other.w_ && self.h_ >= other.h_
    }
}

struct SomePinned {
    data: PhantomData<i32>,
    _pin: PhantomPinned,
}

//#[derive(DerefMut)]
pub(crate) struct NDKCamera {
    pub(crate) self_ptr: AtomicPtr<NDKCamera>,
    cameraMgr_: WACameraManager,
    pub(crate) cameras_: HashMap<CString, CameraId>,
    pub(crate) activeCameraId_: CString,
    cameraFacing_: acamera_metadata_enum_acamera_lens_facing,
    cameraOrientation_: u32,
    requests_: HashMap<PREVIEW_INDICES, CaptureRequestInfo>,
    outputContainer_: WACaptureSessionOutputContainer,
    pub(crate) captureSession_: Option<WACameraCaptureSession>,
    pub(crate) captureSessionState_: CaptureSessionState,
    // set up exposure control
    exposureTime_: i64,
    exposureRange_: std::ops::Range<i64>,
    sensitivity_: i32,
    sensitivityRange_: std::ops::Range<i32>,
    pub(crate) valid_: bool,
    availCallbacks: Option<ACameraManager_AvailabilityCallbacks>,
    stateCallbacks: Option<ACameraDevice_stateCallbacks>,
}

impl NDKCamera {
    pub(crate) fn new() -> Pin<Box<NDKCamera>> {
        debug!("Init NDKCamera");
        let cam_mgr = WACameraManager::create();
        let (active_cam_id, cameras) = NDKCamera::EnumerateCamera(&cam_mgr);
        let outputContainer = WACaptureSessionOutputContainer::create();

        let mut res = NDKCamera {
            self_ptr: AtomicPtr::new(null_mut()),
            cameraMgr_: cam_mgr,
            cameras_: cameras,
            activeCameraId_: active_cam_id,
            cameraFacing_: ACAMERA_LENS_FACING_BACK,
            cameraOrientation_: 0,
            requests_: HashMap::new(),
            outputContainer_: outputContainer,
            captureSession_: None,
            captureSessionState_: MAX_STATE,
            exposureTime_: 0,
            exposureRange_: Range { start: 0, end: 0 },
            sensitivity_: 0,
            sensitivityRange_: Range { start: 0, end: 0 },
            valid_: false,
            availCallbacks: None,
            stateCallbacks: None,
        };
        let mut res_pinned = Box::pin(res);
        let selfptr = unsafe { Pin::as_mut(&mut res_pinned).get_unchecked_mut() as *mut Self };

        debug!("  ------ First ptr is {:?}  -------------", selfptr);

        res_pinned.self_ptr = AtomicPtr::new(selfptr);

        //let mut devListener = NDKCamera::GetDeviceListener(&mut res);
        let mut devListener = res_pinned.GetDeviceListener();

        let id = &res_pinned.activeCameraId_.clone();
        debug!(
            "Check camera with id {:?} ('{}' as str, '{:?}' as number) ",
            id,
            id.to_str().unwrap(),
            id.to_bytes()
        );

        let mut device = res_pinned
            .cameraMgr_
            .openCamera(id.as_ptr(), &mut devListener);
        res_pinned.stateCallbacks.replace(devListener);

        res_pinned
            .cameras_
            .get_mut(id)
            .expect(&*format!(
                "No camera found at activeCameraId_ = {:?} in cameras_. {} at {} ",
                id,
                file!(),
                line!()
            ))
            .device_
            .replace(device);

        //let manListener = NDKCamera::GetManagerListener(&mut res);
        let manListener = res_pinned.GetManagerListener();
        res_pinned.cameraMgr_.registerAvailabilityCallback(
            &manListener as *const ACameraManager_AvailabilityCallbacks,
        );

        res_pinned.availCallbacks.replace(manListener);

        debug!("Getting metadata for camera id '{:?}'", id);
        let mut metadataObj = res_pinned.cameraMgr_.getCameraCharacteristics(&id);

        let lens_exposure_info = metadataObj.getConstEntry(ACAMERA_SENSOR_INFO_EXPOSURE_TIME_RANGE);

        res_pinned.exposureRange_ = Range {
            end: lens_exposure_info.get_i64(0).min(kMaxExposureTime as i64),
            start: lens_exposure_info.get_i64(1).max(kMinExposureTime as i64),
        };
        debug!("Got exposure range {:?}", res_pinned.exposureRange_);

        let lens_sensivity_info = metadataObj.getConstEntry(ACAMERA_SENSOR_INFO_SENSITIVITY_RANGE);
        res_pinned.sensitivityRange_ = Range {
            start: lens_sensivity_info.get_i32(0),
            end: lens_sensivity_info.get_i32(1),
        };
        debug!("Got sensitivity range {:?}", res_pinned.sensitivityRange_);

        res_pinned.valid_ = true;

        res_pinned
    }

    fn get_mut_request(&mut self, idx: PREVIEW_INDICES) -> &mut CaptureRequestInfo {
        self.requests_
            .get_mut(&idx)
            .expect(format!("No {:?} request found in requests_", idx).as_ref())
    }
    fn get_request(&mut self, idx: PREVIEW_INDICES) -> &CaptureRequestInfo {
        self.requests_
            .get(&idx)
            .expect(format!("No {:?} request found in requests_", idx).as_ref())
    }

    fn GetManagerListener(&mut self) -> ACameraManager_AvailabilityCallbacks {
        ACameraManager_AvailabilityCallbacks {
            context: &mut self.self_ptr as *mut _ as *mut c_void,
            onCameraAvailable: Some(OnCameraAvailable),
            onCameraUnavailable: Some(OnCameraUnavailable),
        }
    }
    fn GetDeviceListener(&mut self) -> ACameraDevice_stateCallbacks {
        ACameraDevice_stateCallbacks {
            context: &mut self.self_ptr as *mut _ as *mut c_void,
            onDisconnected: Some(OnDeviceStateChanges),
            onError: Some(OnDeviceErrorChanges),
        }
    }

    /**
     * Process JPG capture SessionCaptureCallback_OnFailed event
     * If this is current JPG capture session, simply resume preview
     * @param session the capture session that failed
     * @param request the capture request that failed
     * @param failure for additional fail info.
     */

    pub(crate) fn OnCaptureFailed(
        &mut self,
        ses: *mut ACameraCaptureSession,
        req: *mut ACaptureRequest,
        failure: *mut ACameraCaptureFailure,
    ) {
        let rq = self
            .requests_
            .get_mut(&JPG_CAPTURE_REQUEST_IDX)
            .expect("No jpeg request found in requests_");
        if self.valid_ && req == rq.request_.load() {
            unsafe_no_check! {
                assert!(
                    (*failure).sequenceId == rq.sessionSequenceId_,
                    "Error jpg sequence id"
                )
            };
            self.StartPreview(true);
        }
    }

    pub(crate) fn OnCaptureSequenceEnd(
        &mut self,
        ses: *mut ACameraCaptureSession,
        sequenceId: i32,
        frameNumber: i64,
    ) {
        debug!("OnCaptureSequenceCompleted successfully called from ffi");
        {
            let jpg_rq = self.get_mut_request(JPG_CAPTURE_REQUEST_IDX);
            if sequenceId != jpg_rq.sessionSequenceId_ {
                return;
            }
        }

        let mut prev_rq = self
            .requests_
            .get_mut(&PREVIEW_REQUEST_IDX)
            .expect(format!("No {:?} request found in requests_", PREVIEW_REQUEST_IDX).as_ref())
            .request_
            .load();
        let prev_sess = self
            .captureSession_
            .as_ref()
            .expect("No captureSession_ found");
        // resume preview
        prev_sess.setRepeatingRequest(null_mut(), 1, &mut prev_rq, null_mut())
    }

    pub(crate) fn OnCaptureSequenceCompleted(
        &mut self,
        ses: *mut ACameraCaptureSession,
        req: *mut ACaptureRequest,
        res: *const ACameraMetadata,
    ) {
        debug!("OnCaptureSequenceCompleted successfully called from ffi");
    }

    pub(crate) fn OnCaptureSequenceStarted(
        &mut self,
        ses: *mut ACameraCaptureSession,
        req: *const ACaptureRequest,
        res: i64,
    ) {
        debug!("OnCaptureSequenceStarted successfully called from ffi");
    }

    /*fn GetDeviceListener() -> &'static ACameraDevice_stateCallbacks {

    }
    fn GetSessionListener() -> &'static ACameraCaptureSession_stateCallbacks {

    }
    fn GetCaptureCallback() -> &'static ACameraCaptureSession_captureCallbacks {

    }
    */
    fn EnumerateCamera(cameraMgr_: &WACameraManager) -> (CString, HashMap<CString, CameraId>) {
        debug!("NDKCamera.EnumerateCamera");

        let cameraIds = cameraMgr_.getCameraIdList();
        debug!("Got cameraIds {:?}", cameraIds);
        //return *(unsafe{ cameraIds.as_ref().unwrap() });

        let mut activeCameraId_ = None;
        let mut cameras_ = HashMap::new();

        for id in cameraIds {
            debug!("Check camera {:?}", id);

            let mut metadataObj = cameraMgr_.getCameraCharacteristics(&id);

            let tags = metadataObj.getAllTags();

            debug!("Got {} tags for camera {:?}", tags.len(), id);

            for tag in tags {
                debug!(
                    "Proceed tag \"{}\"",
                    GetTagStr::<acamera_metadata_tag>(*tag as i64)
                );
                if FromPrimitive::from_u32(tag.clone()) == Some(ACAMERA_LENS_FACING) {
                    let lensInfo = metadataObj.getConstEntry(ACAMERA_LENS_FACING);

                    assert_eq!(
                        &lensInfo.tag, tag,
                        "Tag from generated lensInfo {} is not equals original {} ",
                        &lensInfo.tag, tag
                    );

                    let mut cam = CameraId::new(&id);
                    debug!(
                        "Got camera type {} for id {:?}. Count of metadata elements is {}.",
                        GetTagStr::<acamera_type>(lensInfo.type_ as i64),
                        &cam.id_,
                        lensInfo.count
                    );

                    let facingValue = lensInfo.get_u8(0);
                    debug!("facingValue = {}", facingValue);

                    cam.facing_ =
                        FromPrimitive::from_u8(facingValue).unwrap_or(ACAMERA_LENS_FACING_EXTERNAL); //.unwrap_or_else(|| panic!("Unknown facing value {}", facingValue));
                    cam.owner_ = false;
                    if cam.facing_ == ACAMERA_LENS_FACING_BACK && activeCameraId_.is_none() {
                        debug!("Found back camera with id \"{:?}\"", &id);
                        activeCameraId_.replace(id.clone());
                    } else {
                        debug!(
                            "Facing for camera id \"{:?}\" is {} = \"{}\"",
                            &id,
                            facingValue,
                            GetTagStr::<acamera_metadata_enum_acamera_lens_facing>(
                                facingValue as i64
                            )
                        );
                    }
                    cameras_.insert(id, cam);
                    //let cameras = self.cameras_.clone();
                    break;
                }
            }
        }
        assert!(cameras_.len() > 0, "No Camera Available on the device");
        if activeCameraId_.is_none() {
            // if no back facing camera found, pick up the first one to use...
            let active_camera = cameras_.values().skip(1).last().unwrap();
            activeCameraId_.replace(active_camera.id_.clone());
        }
        (activeCameraId_.unwrap(), cameras_)
    }

    fn GetSensorOrientation(&mut self) -> Option<(acamera_metadata_enum_acamera_lens_facing, u32)> {
        let mut metadata = self
            .cameraMgr_
            .getCameraCharacteristics(&self.activeCameraId_);
        let facing_entry = metadata.getConstEntry(ACAMERA_LENS_FACING);
        self.cameraFacing_ = FromPrimitive::from_u8(facing_entry.get_u8(0)).expect("Unknown value");
        let orientation_entry = metadata.getConstEntry(ACAMERA_SENSOR_ORIENTATION);

        self.cameraOrientation_ = orientation_entry.get_i32(0) as u32;

        return Some((self.cameraFacing_, self.cameraOrientation_));
    }

    /*
     bool NDKCamera::GetSensorOrientation(int32_t* facing, int32_t* angle) {
      if (!cameraMgr_) {
        return false;
      }

      ACameraMetadata* metadataObj;
      ACameraMetadata_const_entry face, orientation;
      CALL_MGR(getCameraCharacteristics(cameraMgr_, activeCameraId_.c_str(),
                                        &metadataObj));
      CALL_METADATA(getConstEntry(metadataObj, ACAMERA_LENS_FACING, &face));
      cameraFacing_ = static_cast<int32_t>(face.data.u8[0]);

      CALL_METADATA(
          getConstEntry(metadataObj, ACAMERA_SENSOR_ORIENTATION, &orientation));

      LOGI("====Current SENSOR_ORIENTATION: %8d", orientation.data.i32[0]);

      ACameraMetadata_free(metadataObj);
      cameraOrientation_ = orientation.data.i32[0];

      if (facing) *facing = cameraFacing_;
      if (angle) *angle = cameraOrientation_;
      return true;
    }

        */

    pub(crate) fn MatchCaptureSizeRequest(
        &mut self,
        requestWidth: i32,
        requestHeight: i32,
        resCaps: &Vec<AIMAGE_FORMATS>,
    ) -> HashMap<AIMAGE_FORMATS, ImageFormat> {
        let mut metadata = self
            .cameraMgr_
            .getCameraCharacteristics(&self.activeCameraId_);

        // Get current orientation
        let orientation_entry = metadata.getConstEntry(ACAMERA_SENSOR_ORIENTATION);
        self.cameraOrientation_ = orientation_entry.get_i32(0) as u32;

        let mut disp = DisplayDimension::new(requestWidth, requestHeight);
        if self.cameraOrientation_ == 90 || self.cameraOrientation_ == 270 {
            disp.Flip();
        }

        debug!("Got orientation = {}", self.cameraOrientation_);

        // Check streams
        let streams_entry = metadata.getConstEntry(ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS);

        // format of the data: format, width, height, input?, type int32
        let mut foundIt = false;
        //let mut foundRes = DisplayDimension::new(640, 480);
        let mut maxSZS: HashMap<AIMAGE_FORMATS, DisplayDimension> = resCaps
            .iter()
            .map(|&k| (k, DisplayDimension::new(640, 480)))
            .collect(); //HashMap::new();

        debug!(
            "Got {} entries for ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS",
            streams_entry.count
        );
        for i in (0..streams_entry.count as isize).step_by(4) {
            let input = streams_entry.get_i32(i + 3);
            debug!("processing {}", i);
            if input != 0 {
                continue;
            }

            let _format = streams_entry.get_i32(i + 0);
            if let Some(format) = FromPrimitive::from_i32(_format) as Option<AIMAGE_FORMATS> {
                if maxSZS.keys().any(|&x| x == format) {
                    let res = DisplayDimension::new(
                        streams_entry.get_i32(i + 1),
                        streams_entry.get_i32(i + 2),
                    );
                    if format == AIMAGE_FORMAT_YUV_420_888 {
                        if !disp.IsSameRatio(&res) {
                            debug!("Display {:?} ratio not equals result {:?} ratio", disp, res);
                        } else {
                            debug!("Display {:?} ratio equals result {:?} ratio", disp, res);
                            debug!("Can use this format for display");
                            if res == disp || (res > maxSZS[&format] && maxSZS[&format] != disp) {
                                foundIt = true;
                                maxSZS.insert(format, res);
                            } else {
                                debug!(
                                    "Found size {:?} isn't greater than {:?}",
                                    res, maxSZS[&format]
                                );
                            }
                        }
                    } else if res > maxSZS[&format] {
                        maxSZS.insert(format, res);
                    }
                } else {
                    debug!("Unknown image format {} at input {}", _format, input);
                }
            }
        }

        maxSZS
            .iter()
            .map(|(&k, &d)| {
                (
                    k,
                    ImageFormat {
                        width: d.w_,
                        height: d.h_,
                        format: k,
                    },
                )
            })
            .collect()
    }

    fn GetSessionListener(&mut self) -> ACameraCaptureSession_stateCallbacks {
        ACameraCaptureSession_stateCallbacks {
            context: &mut self.self_ptr as *mut _ as *mut c_void,
            onClosed: Some(OnSessionClosed),
            onReady: Some(OnSessionReady),
            onActive: Some(OnSessionActive),
        }
    }

    pub(crate) fn CreateSession(
        &mut self,
        mut previewWindow: WANativeWindow,
        _jpgWindow: Option<WANativeWindow>,
        _rawWindow: Option<WANativeWindow>,
        manualPreview: bool,
        imageRotation: i32,
    ) {
        debug!("Start to create new session");
        previewWindow.acquire();
        let inner_prev_win = previewWindow.load();
        let mut previews_tuples = vec![(PREVIEW_REQUEST_IDX, TEMPLATE_PREVIEW, previewWindow)];
        if let Some(jpgWindow) = _jpgWindow {
            previews_tuples.push((JPG_CAPTURE_REQUEST_IDX, TEMPLATE_STILL_CAPTURE, jpgWindow));
        }
        if let Some(rawWindow) = _rawWindow {
            previews_tuples.push((RAW_CAPTURE_REQUEST_IDX, TEMPLATE_STILL_CAPTURE, rawWindow));
        }
        //self.outputContainer_ = WACaptureSessionOutputContainer::create(); //Uncomment if found it must to re-create
        {
            let listener = self.GetSessionListener();
            let camera_dev = self
                .cameras_
                .get(self.activeCameraId_.as_ref())
                .as_ref()
                .expect(&*format!(
                    "No camera found at activeCameraId_ = {:?} in cameras_. {} at {} ",
                    self.activeCameraId_,
                    file!(),
                    line!()
                ))
                .device_
                .as_ref()
                .expect(&*format!(
                    "No device_ found at activeCameraId_ = {:?} in cameras_. {} at {} ",
                    self.activeCameraId_,
                    file!(),
                    line!()
                ));

            for (prev_idx, req_tpl, window) in previews_tuples.into_iter() {
                let target_ = WACameraOutputTarget::create(window.load());

                let mut request = camera_dev.createCaptureRequest(req_tpl);
                request.addTarget(target_.load());

                match prev_idx {
                    JPG_CAPTURE_REQUEST_IDX => {
                        request.setEntry_i32(ACAMERA_JPEG_ORIENTATION, 1, imageRotation);
                    }
                    RAW_CAPTURE_REQUEST_IDX => {
                        request.setEntry_i32(ACAMERA_JPEG_ORIENTATION, 1, imageRotation);
                    }
                    PREVIEW_REQUEST_IDX => {
                        /*
                         * Only preview request is in manual mode, JPG is always in Auto mode
                         * JPG capture mode could also be switch into manual mode and control
                         * the capture parameters, this sample leaves JPG capture to be auto mode
                         * (auto control has better effect than author's manual control)
                         */
                        request.setEntry_u8(
                            ACAMERA_CONTROL_AE_MODE,
                            1,
                            ACAMERA_CONTROL_AE_MODE_ON as u8,
                        );
                        request.setEntry_u8(
                            ACAMERA_CONTROL_AF_MODE,
                            1,
                            ACAMERA_CONTROL_AF_MODE_AUTO as u8,
                        );
                        /*request.setEntry_i32(ACAMERA_SENSOR_SENSITIVITY, 1, self.sensitivity_);
                        request.setEntry_i64(ACAMERA_SENSOR_EXPOSURE_TIME, 1, self.exposureTime_);*/
                    }
                    _ => assert!(false, "Must not be reached, unknown match."),
                }
                let sessionOutput = WACaptureSessionOutput::create(window.load());
                self.outputContainer_.add(sessionOutput.load());

                let rq = CaptureRequestInfo {
                    outputNativeWindow_: window,
                    sessionOutput_: sessionOutput,
                    target_: target_,
                    request_: request,
                    template_: req_tpl,
                    sessionSequenceId_: 0,
                };
                self.requests_.insert(prev_idx, rq);
            }

            let captureSession =
                camera_dev.createCaptureSession(self.outputContainer_.load(), &listener);

            self.captureSession_.replace(captureSession);
        }

        self.captureSessionState_ = CaptureSessionState::READY;
        debug!("New session created");
    }

    pub(crate) fn StartPreview(&mut self, start: bool) {
        if start {
            let mut prev_rq = self.get_mut_request(PREVIEW_REQUEST_IDX).request_.load();
            self.captureSession_
                .as_ref()
                .expect("No captureSession_ found")
                .setRepeatingRequest(null_mut(), 1, &mut prev_rq, null_mut());
        } else if !start && (self.captureSessionState_ == ACTIVE) {
            self.captureSession_
                .as_ref()
                .expect("No captureSession_ found")
                .stopRepeating();
        } else {
            assert!(
                false,
                "Conflict states({}, {:?})",
                start, self.captureSessionState_
            );
        }
    }

    pub(crate) fn TakePhoto(&mut self, idx: PREVIEW_INDICES) -> bool {
        /*if self.captureSessionState_ == CaptureSessionState::ACTIVE {
            unsafe_and_check! { ACameraCaptureSession_stopRepeating(self.captureSession_.expect("No captureSession_ found")) };
        }*/

        self.StartPreview(false);

        //let mut _self: *mut NDKCamera = self as *mut NDKCamera;
        //let mut rq: &mut CaptureRequestInfo =
        //    unsafe { (*_self).get_mut_request(JPG_CAPTURE_REQUEST_IDX) };
        let mut rq = self.get_mut_request(idx).request_.load();

        let ptr = &mut self.self_ptr as *mut _ as *mut c_void;
        let mut callbacks = GetCaptureCallback(ptr);

        let sessSeqId = self
            .captureSession_
            .as_ref()
            .expect("No captureSession_ found")
            .capture(&mut callbacks, 1, &mut rq);

        let mut rq: &mut CaptureRequestInfo = self.get_mut_request(idx);
        rq.sessionSequenceId_ = sessSeqId;
        return true;
    }

    /*
    fn CreateSessionS(previewWindow: &ANativeWindow) {

    }
    fn GetSensorOrientation(facing: &i32, angle: &i32) -> bool {

    }
    fn OnCameraStatusChanged(id: &c_uchar, available: bool){

    }
    fn OnDeviceState(dev: &ACameraDevice) {

    }
    fn OnDeviceError(dev: &ACameraDevice, err: i32){

    }
    fn OnSessionState(ses: &ACameraCaptureSession, state: CaptureSessionState) {

    }
    fn OnCaptureSequenceEnd(session: &ACameraCaptureSession, sequenceId: i32, frameNumber: i64) {

    }
    fn OnCaptureFailed(session: &ACameraCaptureSession, request: &ACaptureRequest, failure: &ACameraCaptureFailure) {

    }
    fn StartPreview(start: bool) {

    }
    fn GetExposureRange(min: &i64, max: &i64, curVal: &i64) -> bool {

    }
    fn GetSensitivityRange(min: &i64, max: &i64, curVal: &i64) -> bool {

    }
    fn UpdateCameraRequestParameter(code: i32, val: i64) {

    }
    */
}

pub(crate) struct CameraId {
    pub(crate) device_: Option<WACameraDevice>,
    id_: CString,
    facing_: acamera_metadata_enum_acamera_lens_facing,
    pub(crate) available_: bool,
    pub(crate) owner_: bool,
}

impl CameraId {
    fn new(id: &CString) -> CameraId {
        CameraId {
            device_: None, //Needs to be open each time we switch device
            id_: id.clone(),
            facing_: ACAMERA_LENS_FACING_FRONT,
            available_: false,
            owner_: false,
        }
    }
}
