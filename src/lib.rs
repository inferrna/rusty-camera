// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#[cfg(target_os = "android")]
extern crate android_logger;
#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate num;
#[macro_use]
extern crate log;
extern crate log_panics;

//use jni::objects::GlobalRef;
//use jni::JNIEnv;
use std::ops::{Deref, Drop};
mod AndroidUnited;
mod android_c_headers;
mod java_glue;
#[macro_use]
mod camera_utils;
mod android_impl;
mod camera_listeners;
mod camera_manager;
mod image_reader;

use crate::android_impl::WANativeWindow;
use crate::camera_manager::PREVIEW_INDICES;
use crate::camera_manager::PREVIEW_INDICES::{
    JPG_CAPTURE_REQUEST_IDX, PREVIEW_REQUEST_IDX, RAW_CAPTURE_REQUEST_IDX,
};
use crate::AndroidUnited::AIMAGE_FORMATS;
use crate::AndroidUnited::AIMAGE_FORMATS::{
    AIMAGE_FORMAT_RAW10, AIMAGE_FORMAT_RAW12, AIMAGE_FORMAT_RAW16, AIMAGE_FORMAT_RGBA_8888,
};
use crate::{
    camera_manager::{ImageFormat, NDKCamera},
    image_reader::ImageReader,
    java_glue::*,
    AndroidUnited::AIMAGE_FORMATS::{AIMAGE_FORMAT_JPEG, AIMAGE_FORMAT_YUV_420_888},
    AndroidUnited::{
        jclass, jint, jobject, ANativeWindow, ANativeWindow_fromSurface, JNIEnv, JNINativeInterface,
    },
};
use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::mem::MaybeUninit;
use std::os::raw::c_void;
use std::pin::Pin;
use std::ptr::{null, null_mut};
use std::rc::Rc;
use std::sync::atomic::{AtomicPtr, Ordering};

static mut JNI: Option<*mut JNIEnv> = None;
static mut INS: Option<jobject> = None;

struct CameraAppEngine {
    camera: Pin<Box<NDKCamera>>,
    env: *mut JNIEnv,
    javaInstance_: jobject,
    requestWidth_: i32,
    requestHeight_: i32,
    target_format: AIMAGE_FORMATS,
    target_format_raw: AIMAGE_FORMATS,
    compatibleCameraRes_: ImageFormat,
    allReaders_: HashMap<AIMAGE_FORMATS, Pin<Box<ImageReader>>>,
}

#[derive(Debug, Copy, Clone)]
struct A {
    a: i32,
    b: i32,
    c: u8,
    d: u16,
}

impl CameraAppEngine {
    pub fn new(w: jint, h: jint) -> CameraAppEngine {
        #[cfg(target_os = "android")]
        android_logger::init_once(
            android_logger::Config::default()
                .with_min_level(log::Level::Debug)
                .with_tag("Hello"),
        );
        log_panics::init(); // log panics rather than printing them
        info!("init log system - done");

        let env = unsafe_no_check!(JNI.unwrap());
        let version = unsafe_no_check! {(**env).GetVersion.expect("Unable to call GetVersion from env!")(env)};
        info!("Got env version {}", version);

        let mut camera = NDKCamera::new();

        let img_formats = vec![
            AIMAGE_FORMAT_YUV_420_888,
            AIMAGE_FORMAT_JPEG,
            AIMAGE_FORMAT_RAW10,
        ];

        let formats = camera.MatchCaptureSizeRequest(w, h, &img_formats);

        let mut res = CameraAppEngine {
            camera,
            env,
            requestWidth_: w,
            requestHeight_: h,
            target_format: img_formats[1],
            target_format_raw: img_formats[2],
            javaInstance_: unsafe_no_check!(INS.unwrap()),
            compatibleCameraRes_: formats[&AIMAGE_FORMAT_YUV_420_888],
            allReaders_: formats
                .iter()
                .map(|(&k, d)| (k, ImageReader::new(d, k)))
                .collect(),
        };

        let offsets: HashMap<AIMAGE_FORMATS, isize> = res
            .allReaders_
            .iter()
            .map(|(&fmt, reader)| (fmt, unsafe { res.offset_from_child(reader) }))
            .collect();
        for (&fmt, reader) in res.allReaders_.iter_mut() {
            let offset = &offsets[&fmt];
            reader.parent_offset.replace(*offset);
        }

        res
    }
    pub unsafe fn offset_from_child(&self, reader: &ImageReader) -> isize {
        let self_ptr = self as *const Self as *const u8;
        let child_ptr = reader as *const ImageReader as *const u8;
        self_ptr.offset_from(child_ptr)
    }

    /*pub(crate) fn CreateSessions(
        &mut self,
        mut previewWindow: WANativeWindow,
        manualPreview: bool,
        imageRotation: i32,
    ) {
        let format_matches: HashMap<PREVIEW_INDICES, AIMAGE_FORMATS> = [
            (PREVIEW_REQUEST_IDX, AIMAGE_FORMAT_YUV_420_888),
            (JPG_CAPTURE_REQUEST_IDX, AIMAGE_FORMAT_JPEG),
            (RAW_CAPTURE_REQUEST_IDX, AIMAGE_FORMAT_RAW12),
        ]
        .iter()
        .collect();



    }*/

    pub fn CreateCameraSession(&mut self, surface: &jobject) {
        debug!("CreateCameraSession");
        //let surface_ = GlobalRef::new(self.env as *mut *const JNINativeInterface_, surface);
        //let mut jpgWindow = MaybeUninit::<ANativeWindow>::uninit();
        let format = self.target_format;
        let jpegWindow = self
            .allReaders_
            .get_mut(&format)
            .expect(&format!("No reader for {:?} found", format))
            .GetNativeWindow();
        let raw_fmt = self.target_format_raw;
        let rawWindow = self
            .allReaders_
            .get_mut(&raw_fmt)
            .expect(&format!("No reader for {:?} found", raw_fmt))
            .GetNativeWindow();
        self.camera.CreateSession(
            WANativeWindow::fromSurface(self.env, *surface),
            Some(jpegWindow),
            Some(rawWindow),
            false,
            0,
        );

        let voidptr = &mut self.camera.self_ptr as *mut _ as *mut c_void;

        let goodptr = voidptr as *mut AtomicPtr<NDKCamera>;

        let goodref = unsafe { goodptr.as_ref().expect("Can't dereference to atomic") };

        let ndkcam = unsafe {
            goodref
                .load(Ordering::SeqCst)
                .as_ref()
                .expect("Can't dereference ndkcam")
        };

        let loaded_ses = ndkcam.captureSession_.as_ref().unwrap().load();
        {
            let unpinned = Pin::as_mut(&mut self.camera).get_mut() as *mut NDKCamera;
            debug!("----- Unpinned = {:?} --------", unpinned);
        }
        debug!("Loaded new CameraSession = {:?}", loaded_ses);
        debug!("activeCameraId_ = {:?}", ndkcam.activeCameraId_);

        self.camera.StartPreview(true);
    }

    pub fn DestroyCameraSession(&mut self) {
        debug!("DestroyCameraSession");
        self.camera.StartPreview(false);
    }

    pub fn getMinimumCompatiblePreviewSize(&self) -> Vec<i32> {
        /*let className = "android/util/Size";
        let mut the_env = unsafe_no_check!(**(self.env));
        let cls = unsafe_no_check!(the_env.FindClass.expect("Can't call to FindClass")(
            self.env,
            CString::new(className).expect(className).as_ptr(),
        ));
        let previewSize = unsafe_no_check!(the_env.NewObject.expect("Can't call to NewObject")(
            self.env,
            cls,
            unsafe_no_check!(the_env.GetMethodID.expect("Can't call to GetMethodID")(
                self.env,
                cls,
                CString::new("<init>").expect("<init>").as_ptr(),
                CString::new("(II)V").expect("(II)V").as_ptr(),
            )),
            self.compatibleCameraRes_.width,
            self.compatibleCameraRes_.height,
        ));*/
        return vec![
            self.compatibleCameraRes_.width,
            self.compatibleCameraRes_.height,
        ];
    }

    pub fn TakePhoto(&mut self) {
        debug!("tap on TakePhoto");
        self.camera.TakePhoto(JPG_CAPTURE_REQUEST_IDX);
    }
    pub fn TakePhotoHDR(&mut self) {
        debug!("tap on TakePhotoHDR");
        self.camera.TakePhoto(JPG_CAPTURE_REQUEST_IDX);
    }
    pub fn TakePhotoRAW(&mut self) {
        debug!("tap on TakePhotoRAW");
        self.camera.TakePhoto(RAW_CAPTURE_REQUEST_IDX);
    }
    fn OnPhotoTaken(&mut self, fileName: String) -> bool {
        //self.camera.StartPreview(true);
        debug!("Saved to {}", fileName);
        true
    }

    // Greeting with full, no-runtime-cost support for newlines and UTF-8
    pub fn greet(to: &str) -> String {
        format!("Hello {} ✋\nIt's a pleasure to meet you!", to)
    }
}

#[no_mangle]
pub extern "system" fn Java_net_akaame_myapplication_MainActivity_PassJNIToRust(
    env: *mut JNIEnv,
    instance: jobject,
    test_int: i32,
) {
    debug_assert!(!env.is_null());
    debug_assert!(!instance.is_null());
    assert_eq!(test_int, 837583);

    unsafe_no_check!(JNI.replace(env));
    unsafe_no_check!(INS.replace(instance));
}
