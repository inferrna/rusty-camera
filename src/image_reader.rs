// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(dead_code)]

use crate::android_impl::{WAImage, WAImageReader, WANativeWindow};
use crate::camera_manager::ImageFormat;
use crate::camera_utils::GetTagStr;
use crate::AndroidUnited::media_status_t::AMEDIA_OK;
use crate::AndroidUnited::ANativeWindow_LegacyFormat::{
    WINDOW_FORMAT_RGBA_8888, WINDOW_FORMAT_RGBX_8888,
};
use crate::AndroidUnited::AIMAGE_FORMATS::{AIMAGE_FORMAT_JPEG, AIMAGE_FORMAT_YUV_420_888};
use crate::AndroidUnited::{
    media_status_t, AImage, AImageCropRect, AImageReader, AImageReader_ImageListener,
    AImageReader_acquireLatestImage, AImageReader_acquireNextImage, AImageReader_getFormat,
    AImageReader_getWindow, AImageReader_new, AImageReader_setImageListener, AImage_delete,
    AImage_getCropRect, AImage_getFormat, AImage_getNumberOfPlanes, AImage_getPlaneData,
    AImage_getPlanePixelStride, AImage_getPlaneRowStride, ANativeWindow, ANativeWindow_Buffer,
    ANativeWindow_LegacyFormat, AIMAGE_FORMATS,
};
use crate::CameraAppEngine;
use chrono::{Datelike, Local, Timelike};
use core::slice;
use futures::executor::{block_on, LocalPool, LocalSpawner};
use futures::task::{LocalSpawnExt, Spawn, SpawnExt};
use num_traits::FromPrimitive;
use std::any::type_name;
use std::fs::File;
use std::io::Write;
use std::marker::PhantomData;
use std::mem::MaybeUninit;
use std::ops::{Shl, Shr, ShrAssign};
use std::os::raw::{c_char, c_void};
use std::path::Path;
use std::pin::Pin;
use std::ptr::{null, null_mut};
use std::rc::Weak;
use std::sync::atomic::AtomicPtr;
use std::{fs, thread};

/**
 * For JPEG capture, captured files are saved under
 *     DirName
 * File names are incrementally appended an index number as
 *     capture0.jpg, capture1.jpg, capture2.jpg
 */
static kDirName: &str = "/sdcard/DCIM/Camera/";
static kFileName: &str = "capture";

type PresentPixel = fn(width: i32, height: i32, x: i32, y: i32, stride: i32) -> usize;

/**
 * MAX_BUF_COUNT:
 *   Max buffers in this ImageReader.
 */
const MAX_BUF_COUNT: usize = 4;

unsafe extern "C" fn OnImageCallback(ctx: *mut c_void, reader: *mut AImageReader) {
    debug!("OnImageCallback fired");
    (*(ctx as *mut ImageReader)).ImageCallback(reader);
}

pub(crate) struct ImageReader {
    presentRotation_: i32,
    reader_: WAImageReader,
    pub(crate) parent_offset: Option<isize>,
    listener: AImageReader_ImageListener,
    format: AIMAGE_FORMATS,
    writing_spawner: LocalSpawner,
}

impl ImageReader {
    pub(crate) fn new(resolution: &ImageFormat, format: AIMAGE_FORMATS) -> Pin<Box<ImageReader>> {
        let mut reader = WAImageReader::new(
            resolution.width,
            resolution.height,
            format as i32,
            MAX_BUF_COUNT as i32,
        );
        let mut result = ImageReader {
            presentRotation_: 0,
            reader_: reader,
            parent_offset: None,
            listener: AImageReader_ImageListener {
                context: null_mut(),
                onImageAvailable: Some(OnImageCallback),
            },
            format,
            writing_spawner: LocalPool::new().spawner(),
        };
        let mut res_pinned = Box::pin(result);
        let selfptr =
            unsafe { Pin::as_mut(&mut res_pinned).get_unchecked_mut() as *mut _ as *mut c_void };
        res_pinned.listener.context = selfptr;
        let listener_ptr = &mut res_pinned.listener as *mut AImageReader_ImageListener;
        res_pinned.reader_.setImageListener(listener_ptr);
        debug!("Created reader for {:?} ", res_pinned.format);
        res_pinned
    }
    fn ImageCallback(&self, reader: *mut AImageReader) {
        debug!("ImageCallback fired to special {:?} reader", self.format);
        let mut _format = 0;
        unsafe_and_check_media! { AImageReader_getFormat(reader, &mut _format) };
        let format: Option<AIMAGE_FORMATS> = FromPrimitive::from_i32(_format);
        if Some(AIMAGE_FORMAT_JPEG) == format {
            let mut _image = MaybeUninit::<*mut AImage>::uninit();
            unsafe_and_check_media! { AImageReader_acquireNextImage(reader, _image.as_mut_ptr()) };
            debug!("Going to init _image");
            let image = unsafe_no_check! { _image.assume_init() };
            // Create a thread and write out the jpeg files
            debug!("Going to spawn WriteFile");
            self.WriteFile(image); //Threaded in original code
        } else {
            assert!(
                false,
                "Can't save format {} recognized as {:?}",
                _format, format
            );
        }
    }

    pub fn GetNativeWindow(&mut self) -> WANativeWindow {
        self.reader_.getWindow()
    }
    /**
     * GetNextImage()
     *   Retrieve the next image in ImageReader's bufferQueue, NOT the last image so
     * no image is skipped. Recommended for batch/background processing.
     */
    pub fn GetNextImage(&mut self) -> Option<WAImage> {
        self.reader_.acquireNextImage()
    }

    /**
     * GetLatestImage()
     *   Retrieve the last image in ImageReader's bufferQueue, deleting images in
     * in front of it on the queue. Recommended for real-time processing.
     */
    pub fn GetLatestImage(&mut self) -> Option<WAImage> {
        self.reader_.acquireLatestImage()
    }

    #[inline]
    fn YUV2RGB(_nY: i32, _nU: i32, _nV: i32) -> u32 {
        const K_MAX_CHANNEL_VALUE: i32 = 262143;
        let nY = (_nY - 16).max(0);
        let nU = _nU - 128;
        let nV = _nV - 128;

        // This is the floating point equivalent. We do the conversion in integer
        // because some Android devices do not have floating point in hardware.
        // nR = (int)(1.164 * nY + 1.596 * nV);
        // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
        // nB = (int)(1.164 * nY + 2.018 * nU);

        let nR = (1192 * nY + 1634 * nV)
            .min(K_MAX_CHANNEL_VALUE)
            .max(0)
            .shr(10)
            & 0xff;
        let nG = (1192 * nY - 833 * nV - 400 * nU)
            .min(K_MAX_CHANNEL_VALUE)
            .max(0)
            .shr(10)
            & 0xff;
        let nB = (1192 * nY + 2066 * nU)
            .min(K_MAX_CHANNEL_VALUE)
            .max(0)
            .shr(10)
            & 0xff;

        0xff000000u32 | (nR as u32).shl(16) | (nG as u32).shl(8) | nB as u32
    }

    /**
     * Convert yuv image inside AImage into ANativeWindow_Buffer
     * ANativeWindow_Buffer format is guaranteed to be
     *      WINDOW_FORMAT_RGBX_8888
     *      WINDOW_FORMAT_RGBA_8888
     * @param buf a {@link ANativeWindow_Buffer } instance, destination of
     *            image conversion
     * @param image a {@link AImage} instance, source of image conversion.
     *            it will be deleted via {@link AImage_delete}
     */
    fn DisplayImage(&self, buf: *mut ANativeWindow_Buffer, image: *mut AImage) -> bool {
        let format = unsafe_no_check! { (*buf).format };
        assert!(
            format == WINDOW_FORMAT_RGBX_8888 as i32 || format == WINDOW_FORMAT_RGBA_8888 as i32,
            "Not supported buffer format: {:?}",
            GetTagStr::<ANativeWindow_LegacyFormat>(format as i64)
        );

        let mut _srcFormat: i32 = -1;

        unsafe_and_check_media! {AImage_getFormat(image, &mut _srcFormat)};
        let srcFormat = FromPrimitive::from_i32(_srcFormat);
        assert_eq!(
            Some(AIMAGE_FORMAT_YUV_420_888),
            srcFormat,
            "Failed to get format, got: {}",
            GetTagStr::<AIMAGE_FORMATS>(_srcFormat as i64)
        );

        let mut srcPlanes = 0;
        unsafe_and_check_media! { AImage_getNumberOfPlanes(image, &mut srcPlanes) };
        assert_eq!(srcPlanes, 3, "Is not 3 planes");

        let idf: PresentPixel = match self.presentRotation_ {
            0 => |w, h, x, y, s| (x + y * s) as usize,
            90 => |w, h, x, y, s| ((h - y - 1) + x * s) as usize,
            180 => |w, h, x, y, s| ((h - y - 1) * s + (w - 1 - x)) as usize,
            270 => |w, h, x, y, s| (y + (w - 1 - x) * s) as usize,
            _ => {
                assert!(
                    false,
                    "NOT recognized display rotation: {}",
                    self.presentRotation_
                );
                |_, _, _, _, _| 0
            }
        };
        self.PresentImage(buf, image, idf);
        unsafe_no_check! { AImage_delete(image) };

        true
    }

    /**
     * PresentImage()
     *   Converting yuv to RGB
     *   No rotation: (x,y) --> (x, y)
     *   Refer to:
     * https://mathbits.com/MathBits/TISection/Geometry/Transformations2.htm
     */
    fn PresentImage(
        &self,
        _buf: *mut ANativeWindow_Buffer,
        image: *mut AImage,
        idx_func: PresentPixel,
    ) {
        let mut _srcRect = MaybeUninit::<AImageCropRect>::uninit();
        unsafe_and_check_media! { AImage_getCropRect(image, _srcRect.as_mut_ptr()) };
        let mut srcRect = unsafe_no_check! { _srcRect.assume_init() };

        let mut buf = unsafe { (*_buf) };

        let (mut yStride, mut uvStride) = (0i32, 0i32);
        let (mut _yPixel, mut _uPixel, mut _vPixel) = (
            MaybeUninit::<*mut u8>::uninit(),
            MaybeUninit::<*mut u8>::uninit(),
            MaybeUninit::<*mut u8>::uninit(),
        );
        let (mut yLen, mut uLen, mut vLen) = (0i32, 0i32, 0i32);
        unsafe_and_check_media! { AImage_getPlaneRowStride(image, 0, &mut yStride) };
        unsafe_and_check_media! { AImage_getPlaneRowStride(image, 1, &mut uvStride) };
        unsafe_and_check_media! { AImage_getPlaneData(image, 0, _yPixel.as_mut_ptr(), &mut yLen) };
        unsafe_and_check_media! { AImage_getPlaneData(image, 1, _vPixel.as_mut_ptr(), &mut vLen) };
        unsafe_and_check_media! { AImage_getPlaneData(image, 2, _uPixel.as_mut_ptr(), &mut uLen) };
        let mut uvPixelStride = 0i32;
        unsafe_and_check_media! { AImage_getPlanePixelStride(image, 1, &mut uvPixelStride) };

        let height: i32 = buf.height.min(srcRect.bottom - srcRect.top);
        let width: i32 = buf.width.min(srcRect.right - srcRect.left);

        let (mut yPixel, mut uPixel, mut vPixel) = unsafe_no_check! {
            (
                slice::from_raw_parts(_yPixel.assume_init(), yLen as usize),
                slice::from_raw_parts(_uPixel.assume_init(), uLen as usize),
                slice::from_raw_parts(_vPixel.assume_init(), vLen as usize),
            )
        };

        let out: &mut [u32] = unsafe_no_check! {
            slice::from_raw_parts_mut(buf.bits as *mut u32, (buf.width * buf.height) as usize)
        };
        for y in 0..height {
            let pY = &yPixel[(yStride * (y + srcRect.top) + srcRect.left) as usize..];

            let uv_row_start = uvStride * (y + srcRect.top).shr(1);
            let pU = &uPixel[(uv_row_start + srcRect.left.shr(1)) as usize..];
            let pV = &vPixel[(uv_row_start + srcRect.left.shr(1)) as usize..];

            for x in 0..width {
                let uv_offset = x.shr(1) * uvPixelStride;
                let idx = idx_func(width, height, x, y, buf.stride);
                out[idx] = Self::YUV2RGB(
                    pY[x as usize] as i32,
                    pU[uv_offset as usize] as i32,
                    pV[uv_offset as usize] as i32,
                );
            }
        }
    }
    fn SetPresentRotation(&mut self, angle: i32) {
        self.presentRotation_ = angle;
    }
    /**
     * Write out jpeg files to kDirName directory
     * @param image point capture jpg image
     */
    fn WriteFile(&self, image: *mut AImage) {
        let mut planeCount = 0;
        unsafe_and_check_media! { AImage_getNumberOfPlanes(image, &mut planeCount) };
        assert_eq!(planeCount, 1);

        let _parent = unsafe { self.get_parent::<CameraAppEngine>() };

        let now = Local::now();
        let fileName = format!(
            "{}{}{}-{:02}-{:02}_{}-{}-{}.jpg",
            kDirName,
            kFileName,
            now.year(),
            now.month(),
            now.day(),
            now.hour(),
            now.minute(),
            now.second()
        );

        let fnm = fileName.clone();
        let atomicImg = AtomicPtr::new(image);
        thread::spawn(move || ImageReader::write_data(fnm, atomicImg));

        //block_on(ImageReader::write_data(fileName.clone(), data)); //Blocking method. Spawn fails with 'Unable to spawn: SpawnError("shutdown")' even on trivial say_hello function
        //self.writing_spawner.spawn(ImageReader::write_data(fileName.clone(), data.clone())).expect("Unable to spawn file writer");
        //self.writing_spawner.spawn(ImageReader::say_hello()).expect("Unable to spawn");
        if let Some(mut parent) = _parent {
            parent.OnPhotoTaken(fileName);
        } else {
            debug!("No parent found at offset!");
        }
        debug!("Saved ");
    }

    async fn say_hello() {
        debug!("Hello, so you asked.");
    }

    fn write_data(fileName: String, mut image: AtomicPtr<AImage>) {
        if !Path::new(kDirName).exists() {
            debug!("Directory {} not found. Trying to create.", kDirName);
            fs::create_dir_all(kDirName)
                .expect(format!("Unable to create directory {} ", kDirName).as_ref());
        } else {
            debug!("Directory {} found. No need to create.", kDirName);
        }

        let mut _data = MaybeUninit::<*mut u8>::uninit();
        let mut len = 0;
        unsafe_and_check_media! { AImage_getPlaneData(*(image.get_mut()), 0, _data.as_mut_ptr(), &mut len) };
        let data = unsafe_no_check! { slice::from_raw_parts(_data.assume_init(), len as usize) };

        debug!("Going to create file {}", fileName);
        let mut file = File::create(fileName.clone())
            .expect(format!("Unable to create file {} ", fileName).as_ref());
        debug!("Going to write file {}", fileName);
        let res = file.write(data).expect("Failed to save file ");
        debug!("Written {} bytes", res);
        unsafe_no_check! { AImage_delete(*(image.get_mut())) };
    }

    unsafe fn get_parent<T>(&self) -> Option<&mut T> {
        debug!("Unsafe parent of type {:?} by offset", stringify!(T));
        match self.parent_offset {
            Some(poff) => {
                let ptr_c = self as *const Self as *const u8;
                let ptr_p = ptr_c.offset(poff) as *mut T;
                ptr_p.as_mut()
            }
            None => None,
        }
    }
}
