// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

use crate::camera_manager::{CameraId, CaptureSessionState, NDKCamera};
#[macro_use]
use crate::camera_utils::GetTagStr;
use crate::AndroidUnited::{
    ACameraCaptureFailure, ACameraCaptureSession, ACameraCaptureSession_captureCallbacks,
    ACameraDevice, ACameraDevice_close, ACameraDevice_getId, ACameraMetadata, ACameraWindowType,
    ACaptureRequest, CamErrType,
};
use num_traits::FromPrimitive;
use std::cell::RefMut;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::ffi::{c_void, CStr};
use std::os::raw::c_char;
use std::sync::atomic::{AtomicPtr, Ordering};

extern "C" fn OnSessionState(
    ndkcam: *mut AtomicPtr<NDKCamera>,
    ses: *mut ACameraCaptureSession,
    state: CaptureSessionState,
) {
    debug!("OnSessionState {:?}", &state);
    if unsafe { ses.is_null() } {
        debug!("CaptureSession is NULL");
        return;
    }
    let mut ndkcam = unsafe {
        (*ndkcam)
            .load(Ordering::SeqCst)
            .as_mut()
            .expect("Can't dereference ndkcam")
    };
    if let Some(mut _ourses) = ndkcam.captureSession_.as_mut() {
        let ourses = _ourses.load();
        if unsafe { ourses == ses } {
            assert!(
                (state as isize) < CaptureSessionState::MAX_STATE as isize,
                "Wrong state {:?}",
                state
            );
            debug!("State to {:?}", state);
            ndkcam.captureSessionState_ = state;
        } else {
            debug!(
                "Not our session: our is {:?}, got {:?}. activeCameraId_ = {{:?}}",
                &ourses,
                &ses,
                //&ndkcam.activeCameraId_
            );
        }
    } else {
        debug!("No session");
    }
}

// Camera state callbacks

pub unsafe extern "C" fn OnDeviceErrorChanges(
    ndkcam: *mut c_void,
    dev: *mut ACameraDevice,
    _err: i32,
) {
    OnDeviceError(ndkcam as *mut AtomicPtr<NDKCamera>, dev, _err);
}

unsafe extern "C" fn OnDeviceError(
    ndkcam: *mut AtomicPtr<NDKCamera>,
    dev: *mut ACameraDevice,
    _err: i32,
) {
    let id = unsafe { ACameraDevice_getId(dev) };
    let idCStr = CStr::from_ptr(id);
    let err: CamErrType =
        FromPrimitive::from_i32(_err).expect(format!("Unknown error {}", _err).as_ref());
    debug!("CameraDevice {:?} is in error {:?}", idCStr, err);

    let cam: &mut CameraId = (*(*ndkcam).load(Ordering::SeqCst))
        .cameras_
        .get_mut(idCStr)
        .expect(format!("no camera found at id {:?}", idCStr).as_ref());

    match err {
        CamErrType::ERROR_CAMERA_IN_USE => unsafe {
            cam.available_ = false;
            cam.owner_ = false;
        },
        CamErrType::ERROR_CAMERA_SERVICE
        | CamErrType::ERROR_CAMERA_DEVICE
        | CamErrType::ERROR_CAMERA_DISABLED
        | CamErrType::ERROR_MAX_CAMERAS_IN_USE => unsafe {
            cam.available_ = false;
            cam.owner_ = false;
        },
    }
}

pub unsafe extern "C" fn OnDeviceStateChanges(ndkcam: *mut c_void, dev: *mut ACameraDevice) {
    debug!("OnDeviceState");
    OnDeviceState(ndkcam as *mut AtomicPtr<NDKCamera>, dev);
}

unsafe extern "C" fn OnDeviceState(ndkcam: *mut AtomicPtr<NDKCamera>, dev: *mut ACameraDevice) {
    let id = unsafe { ACameraDevice_getId(dev) };
    let idCStr = CStr::from_ptr(id);
    debug!("CameraDevice {:?} is disconnected", idCStr);
    let cam = (*(*ndkcam).load(Ordering::SeqCst))
        .cameras_
        .get_mut(idCStr)
        .expect(format!("no camera found at id {:?}", idCStr).as_ref());
    cam.available_ = false;
    cam.device_
        .as_mut()
        .expect(format!("no device found in camera at id {:?}", idCStr).as_ref())
        .close();
}

// Camera avail callbacks

unsafe extern "C" fn OnCameraStatusChanged(
    ndkcam: *mut AtomicPtr<NDKCamera>,
    id: *const c_char,
    available: bool,
) {
    let idCStr = CStr::from_ptr(id);
    debug!("Camera {:?} status change to {}", idCStr, available);
    if unsafe_no_check!((*(*ndkcam).load(Ordering::SeqCst)).valid_) {
        unsafe {
            (*(*ndkcam).load(Ordering::SeqCst))
                .cameras_
                .get_mut(idCStr)
                .expect(format!("Can't get camera at id {:?}", idCStr).as_ref())
                .available_ = available
        }
    }
}

pub unsafe extern "C" fn OnCameraUnavailable(ndkcam: *mut c_void, id: *const c_char) {
    debug!("OnCameraUnavailable");
    OnCameraStatusChanged(ndkcam as *mut AtomicPtr<NDKCamera>, id, false);
}

pub unsafe extern "C" fn OnCameraAvailable(ndkcam: *mut c_void, id: *const c_char) {
    debug!("OnCameraAvailable");
    OnCameraStatusChanged(ndkcam as *mut AtomicPtr<NDKCamera>, id, true);
}

// CaptureSession state callbacks

pub extern "C" fn OnSessionClosed(ndkcam: *mut c_void, ses: *mut ACameraCaptureSession) {
    debug!("session {:?} closed", ses);
    OnSessionState(
        ndkcam as *mut AtomicPtr<NDKCamera>,
        ses,
        CaptureSessionState::CLOSED,
    );
}
pub extern "C" fn OnSessionReady(ndkcam: *mut c_void, ses: *mut ACameraCaptureSession) {
    debug!("session {:?} ready", ses);
    OnSessionState(
        ndkcam as *mut AtomicPtr<NDKCamera>,
        ses,
        CaptureSessionState::READY,
    );
}
pub extern "C" fn OnSessionActive(ndkcam: *mut c_void, ses: *mut ACameraCaptureSession) {
    debug!("session {:?} active", ses);
    OnSessionState(
        ndkcam as *mut AtomicPtr<NDKCamera>,
        ses,
        CaptureSessionState::ACTIVE,
    );
}

pub unsafe extern "C" fn SessionCaptureCallback_OnFailed(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    req: *mut ACaptureRequest,
    failure: *mut ACameraCaptureFailure,
) {
    debug!("Capture for session {:?} failed", ses);
    let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureFailed(ses, req, failure); //Must be threaded
}

pub unsafe extern "C" fn SessionCaptureCallback_OnSequenceEnd(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    sequenceId: i32,
    frameNumber: i64,
) {
    debug!("Capture sequence for session {:?} ended", ses);
    let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureSequenceEnd(ses, sequenceId, frameNumber);
    //Must be threaded
}

pub unsafe extern "C" fn SessionCaptureCallback_OnSequenceAborted(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    sequenceId: i32,
) {
    debug!("Capture sequence for session {:?} aborted", ses);
    let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureSequenceEnd(ses, sequenceId, -1);
    //Must be threaded
}

pub unsafe extern "C" fn SessionCaptureCallback_OnCaptureCompleted(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    req: *mut ACaptureRequest,
    res: *const ACameraMetadata,
) {
    debug!("Capture for session {:?} completed", ses);
    let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureSequenceCompleted(ses, req, res);
    //Must be threaded
}

pub unsafe extern "C" fn SessionCaptureCallback_OnCaptureProgressed(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    req: *mut ACaptureRequest,
    res: *const ACameraMetadata,
) {
    debug!("Capture for session {:?} progressed", ses);
    /*let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureSequenceCompleted(ses, req, res);*/
    //Must be threaded
}

unsafe extern "C" fn SessionCaptureCallback_OnCaptureBufferLost(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    req: *mut ACaptureRequest,
    win: *mut ACameraWindowType,
    frameNumber: i64,
) {
    debug!("Lost buffer for session {:?}", ses);
}

pub unsafe extern "C" fn SessionCaptureCallback_OnCaptureStarted(
    _ndkcam: *mut c_void,
    ses: *mut ACameraCaptureSession,
    req: *const ACaptureRequest,
    res: i64,
) {
    debug!("Started capture for session {:?}", ses);
    let mut ndkcam = _ndkcam as *mut AtomicPtr<NDKCamera>;
    (*(*ndkcam).load(Ordering::SeqCst)).OnCaptureSequenceStarted(ses, req, res);
    //Must be threaded
}

pub extern "C" fn GetCaptureCallback(
    ndkcam: *mut c_void,
) -> ACameraCaptureSession_captureCallbacks {
    ACameraCaptureSession_captureCallbacks {
        context: ndkcam,
        onCaptureStarted: Some(SessionCaptureCallback_OnCaptureStarted),
        onCaptureProgressed: Some(SessionCaptureCallback_OnCaptureProgressed),
        onCaptureCompleted: Some(SessionCaptureCallback_OnCaptureCompleted),
        onCaptureFailed: Some(SessionCaptureCallback_OnFailed),
        onCaptureSequenceCompleted: Some(SessionCaptureCallback_OnSequenceEnd),
        onCaptureSequenceAborted: Some(SessionCaptureCallback_OnSequenceAborted),
        onCaptureBufferLost: Some(SessionCaptureCallback_OnCaptureBufferLost),
    }
}
