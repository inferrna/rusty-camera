// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

extern crate num_traits;
#[macro_use]
use num_traits::FromPrimitive;
use crate::AndroidUnited::acamera_metadata_tag::ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS;
use crate::AndroidUnited::acamera_type::{
    ACAMERA_NUM_TYPES, ACAMERA_TYPE_BYTE, ACAMERA_TYPE_DOUBLE, ACAMERA_TYPE_FLOAT,
    ACAMERA_TYPE_INT32, ACAMERA_TYPE_INT64, ACAMERA_TYPE_RATIONAL,
};
use crate::AndroidUnited::{
    acamera_metadata_tag_t, ACameraIdList, ACameraManager, ACameraManager_deleteCameraIdList,
    ACameraManager_getCameraCharacteristics, ACameraManager_getCameraIdList, ACameraMetadata,
    ACameraMetadata_free, ACameraMetadata_getAllTags, ACameraMetadata_getConstEntry,
    ACameraMetadata_rational, ACaptureRequest, ACaptureRequest_getAllTags,
    ACaptureRequest_getConstEntry,
};
use crate::{
    AndroidUnited::acamera_metadata_tag::ACAMERA_LENS_FACING,
    AndroidUnited::camera_status_t::ACAMERA_OK,
    AndroidUnited::{acamera_metadata_tag, camera_status_t, ACameraMetadata_const_entry},
};
use std::fmt::Debug;
use std::mem::MaybeUninit;

#[macro_export]
macro_rules! unsafe_and_check_camera {
    ( $s:stmt ) => {
        debug!(
            "Call to unsafe {:?} from {} at {}",
            stringify!($s),
            file!(),
            line!()
        );
        let status: crate::AndroidUnited::camera_status_t = unsafe { $s };
        assert_eq!(
            status,
            crate::AndroidUnited::camera_status_t::ACAMERA_OK,
            "Got status '{:?}' when call {:?} from {} at {}",
            status,
            stringify!($s),
            file!(),
            line!()
        );
    };
}
#[macro_export]
macro_rules! unsafe_no_check {
    ( $s:stmt ) => {{
        debug!(
            "Call to unsafe '{:?}' from {} at {}",
            stringify!($s),
            file!(),
            line!()
        );
        let res = unsafe { $s };
        debug!("Successful '{:?}'", stringify!($s));
        res
    }};
}
#[macro_export]
macro_rules! unsafe_and_check_zero {
    ( $s:stmt ) => {
        debug!(
            "Call to unsafe '{:?}' from {} at {}",
            stringify!($s),
            file!(),
            line!()
        );
        let status = unsafe { $s };
        assert_eq!(
            status,
            0,
            "Got status '{}' when called '{:?}' from {} at {}",
            status,
            stringify!($s),
            file!(),
            line!()
        );
    };
}
#[macro_export]
macro_rules! unsafe_and_check_media {
    ( $s:stmt ) => {
        debug!(
            "Call to unsafe {:?} from {} at {}",
            stringify!($s),
            file!(),
            line!()
        );
        let status: crate::AndroidUnited::media_status_t = unsafe { $s };
        assert_eq!(
            status,
            crate::AndroidUnited::media_status_t::AMEDIA_OK,
            "Got status '{:?}' when call {:?} from {} at {}",
            status,
            stringify!($s),
            file!(),
            line!()
        );
    };
}

trait Enums {}
impl Enums for acamera_metadata_tag {}
impl Enums for u32 {}

pub(crate) fn GetTagStr<T: FromPrimitive + Debug>(tag: i64) -> String {
    let name: Option<T> = FromPrimitive::from_i64(tag);
    match name {
        Some(value) => format!("{:08}: {:?}", tag, value),
        None => format!("unknown tag {:08}", tag),
    }
}

fn PrintMetadataTags(entries: i32, pTags: &[u32]) {
    debug!("MetadataTag (start):");
    for idx in 0..entries as usize {
        debug!("{}", GetTagStr::<acamera_metadata_tag>(pTags[idx] as i64));
    }
    debug!("MetadataTag (end)");
}

fn PrintLensFacing(lens: &ACameraMetadata_const_entry) {
    let tag: Option<acamera_metadata_tag> = FromPrimitive::from_u32(lens.tag);
    let name = GetTagStr::<acamera_metadata_tag>(lens.tag as i64);
    debug!(
        "LensFacing: tag({}) = {}, type({}), count({}), val({:?})",
        lens.tag,
        name,
        lens.type_,
        lens.count,
        lens.get_u8(0)
    );
    assert_eq!(
        tag,
        Some(ACAMERA_LENS_FACING),
        "Wrong tag({}) of {} to {}",
        lens.tag,
        name,
        "__FUNCTION__"
    );
}

/*
 * Stream_Configuration is in format of:
 *    format, width, height, input?
 *    ACAMERA_TYPE_INT32 type
 */
fn PrintStreamConfigurations(val: &ACameraMetadata_const_entry) {
    //#define MODE_LABLE "ModeInfo:"
    let tagName = GetTagStr::<acamera_metadata_tag_t>(val.tag as i64);
    assert_eq!(
        (val.count % 4),
        0,
        "STREAM_CONFIGURATION ({}) should multiple of 4",
        val.count
    );
    assert_eq!(
        val.type_, ACAMERA_TYPE_INT32 as u8,
        "STREAM_CONFIGURATION TYPE({}) is not ACAMERA_TYPE_INT32(1)",
        val.type_
    );
    debug!("{} -- {}:", tagName, "ModeInfo:");
    for i in (0..val.count as isize).step_by(4) {
        debug!(
            "{}: {} x {} {}",
            GetTagStr::<acamera_metadata_tag_t>(val.get_i32(i) as i64),
            val.get_i32(i + 1),
            val.get_i32(i + 2),
            match val.get_i32(i + 3) > 0 {
                true => "INPUT",
                _ => "OUTPUT",
            }
        );
    }
    //#undef MODE_LABLE
}

fn PrintTagVal(printLabel: &str, val: &ACameraMetadata_const_entry) {
    if val.tag == ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS as u32 {
        PrintStreamConfigurations(val);
        return;
    }
    let name = GetTagStr::<acamera_metadata_tag_t>(val.tag as i64);
    for i in 0..val.count as isize {
        match FromPrimitive::from_u8(val.type_) {
            Some(ACAMERA_TYPE_INT32) => debug!("{} {}: {}", printLabel, name, val.get_i32(i)),
            Some(ACAMERA_TYPE_BYTE) => debug!("{} {}: {}", printLabel, name, val.get_u8(i)),
            Some(ACAMERA_TYPE_INT64) => debug!("{} {}: {}", printLabel, name, val.get_i64(i)),
            Some(ACAMERA_TYPE_FLOAT) => debug!("{} {}: {}", printLabel, name, val.get_f(i)),
            Some(ACAMERA_TYPE_DOUBLE) => debug!("{} {}: {}", printLabel, name, val.get_i64(i)),
            Some(ACAMERA_TYPE_RATIONAL) => debug!(
                "{} {}: {}/{}",
                printLabel,
                name,
                val.get_r(i).numerator,
                val.get_r(i).denominator
            ),
            Some(ACAMERA_NUM_TYPES) => panic!("Bad tag value type: ACAMERA_NUM_TYPES"),
            None => panic!("Unknown tag value type: {}", val.type_),
        }
    }
}

/*
 * PrintCamera():
 *   Enumerate existing camera and its metadata.
 */
fn PrintCameras(cmrMgr: *mut ACameraManager) {
    if unsafe { cmrMgr.is_null() } {
        debug!("Null object provided as cmrMgr. Exiting PrintCameras");
        return ();
    }

    let mut _cameraIds = MaybeUninit::<*mut ACameraIdList>::uninit();
    unsafe_and_check_camera!(ACameraManager_getCameraIdList(
        cmrMgr,
        _cameraIds.as_mut_ptr()
    ));
    let mut cameraIds = unsafe { *(_cameraIds.assume_init()) };

    for i in 0..cameraIds.numCameras as isize {
        let id: *const char = unsafe { cameraIds.cameraIds.offset(i) as *const char };
        debug!(
            "=====cameraId = {}, cameraName = {}=====",
            i,
            unsafe { *id }.to_string()
        );

        let mut _metadataObj = MaybeUninit::<*mut ACameraMetadata>::uninit();
        unsafe_and_check_camera!(ACameraManager_getCameraCharacteristics(
            cmrMgr,
            id as *const u8,
            _metadataObj.as_mut_ptr()
        ));
        let metadataObj = unsafe { _metadataObj.assume_init() };

        let mut count = 0i32;
        let mut _tags = MaybeUninit::<*const u32>::uninit();
        unsafe_and_check_camera!(ACameraMetadata_getAllTags(
            metadataObj,
            &mut count,
            _tags.as_mut_ptr()
        ));
        let tags = unsafe { _tags.assume_init() };

        for tagIdx in 0..count as isize {
            let mut _val = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
            let tag = unsafe { *tags.offset(i) };
            let status: crate::AndroidUnited::camera_status_t = unsafe_no_check!(
                ACameraMetadata_getConstEntry(metadataObj, tag, _val.as_mut_ptr())
            );
            if status != ACAMERA_OK {
                debug!(
                    "Unsupported Tag: {}",
                    GetTagStr::<acamera_metadata_tag_t>(tag as i64)
                );
                continue;
            }
            let val = unsafe { _val.assume_init() };
            PrintTagVal("Camera Tag:", &val);

            if let Some(ACAMERA_LENS_FACING) = FromPrimitive::from_u32(tag) {
                PrintLensFacing(&val);
            }
        }
        unsafe_no_check! { ACameraMetadata_free(metadataObj) };
    }

    unsafe_no_check! { ACameraManager_deleteCameraIdList(&mut cameraIds) };
}

fn PrintRequestMetadata(req: *const ACaptureRequest) {
    if unsafe { req.is_null() } {
        debug!("Null object provided as req. Exiting PrintCameras");
        return ();
    }
    let mut count = 0i32;
    let mut _tags = MaybeUninit::<*const u32>::uninit();
    unsafe_and_check_camera!(ACaptureRequest_getAllTags(
        req,
        &mut count,
        _tags.as_mut_ptr()
    ));
    let tags = unsafe { _tags.assume_init() };
    for idx in 0..count as isize {
        let mut _val = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
        let tag = unsafe { *tags.offset(idx) };
        let status: crate::AndroidUnited::camera_status_t =
            unsafe_no_check!(ACaptureRequest_getConstEntry(req, tag, _val.as_mut_ptr()));
        let name = GetTagStr::<acamera_metadata_tag_t>(tag as i64);
        let val = unsafe { _val.assume_init() };
        for i in 0..val.count as isize {
            match FromPrimitive::from_u8(val.type_) {
                Some(ACAMERA_TYPE_INT32) => debug!("Capture Tag {}: {}", name, val.get_i32(i)),
                Some(ACAMERA_TYPE_BYTE) => debug!("Capture Tag {}: {}", name, val.get_u8(i)),
                Some(ACAMERA_TYPE_INT64) => debug!("Capture Tag {}: {}", name, val.get_i64(i)),
                Some(ACAMERA_TYPE_FLOAT) => debug!("Capture Tag {}: {}", name, val.get_f(i)),
                Some(ACAMERA_TYPE_DOUBLE) => debug!("Capture Tag {}: {}", name, val.get_i64(i)),
                Some(ACAMERA_TYPE_RATIONAL) => debug!(
                    "Capture Tag {}: {}/{}",
                    name,
                    val.get_r(i).numerator,
                    val.get_r(i).denominator
                ),
                Some(ACAMERA_NUM_TYPES) => panic!("Bad tag value type: ACAMERA_NUM_TYPES"),
                None => panic!("Unknown tag value type: {}", val.type_),
            }
        }
    }
}
/*
/*
 * CameraDevice error state translation, used in
 *     ACameraDevice_ErrorStateCallback
 */
using DEV_ERROR_PAIR = std::pair<int, const char*>;
static std::vector<DEV_ERROR_PAIR> devErrors{
    MAKE_PAIR(ERROR_CAMERA_IN_USE),   MAKE_PAIR(ERROR_MAX_CAMERAS_IN_USE),
    MAKE_PAIR(ERROR_CAMERA_DISABLED), MAKE_PAIR(ERROR_CAMERA_DEVICE),
    MAKE_PAIR(ERROR_CAMERA_SERVICE),
};

const char* GetCameraDeviceErrorStr(int err) {
  return GetPairStr<int>(err, devErrors);
}
void PrintCameraDeviceError(int err) {
  LOGI("CameraDeviceError(%#x): %s", err, GetCameraDeviceErrorStr(err));
}
*/
