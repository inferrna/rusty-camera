// Copyright (C) 2019-2020 Ilia Efimov
//
// This file is part of the RustyCam app. This app is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This app is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
use crate::AndroidUnited::media_status_t::AMEDIA_OK;
use crate::AndroidUnited::{
    acamera_metadata_tag, jobject, ACameraCaptureSession, ACameraCaptureSession_abortCaptures,
    ACameraCaptureSession_capture, ACameraCaptureSession_captureCallbacks,
    ACameraCaptureSession_close, ACameraCaptureSession_getDevice,
    ACameraCaptureSession_logicalCamera_capture,
    ACameraCaptureSession_logicalCamera_captureCallbacks,
    ACameraCaptureSession_logicalCamera_setRepeatingRequest,
    ACameraCaptureSession_setRepeatingRequest, ACameraCaptureSession_stateCallbacks,
    ACameraCaptureSession_stopRepeating, ACameraCaptureSession_updateSharedOutput, ACameraDevice,
    ACameraDevice_StateCallbacks, ACameraDevice_close, ACameraDevice_createCaptureRequest,
    ACameraDevice_createCaptureRequest_withPhysicalIds, ACameraDevice_createCaptureSession,
    ACameraDevice_createCaptureSessionWithSessionParameters, ACameraDevice_getId,
    ACameraDevice_isSessionConfigurationSupported, ACameraDevice_request_template, ACameraIdList,
    ACameraManager, ACameraManager_AvailabilityCallbacks,
    ACameraManager_ExtendedAvailabilityCallbacks, ACameraManager_create, ACameraManager_delete,
    ACameraManager_getCameraCharacteristics, ACameraManager_getCameraIdList,
    ACameraManager_openCamera, ACameraManager_registerAvailabilityCallback,
    ACameraManager_registerExtendedAvailabilityCallback,
    ACameraManager_unregisterAvailabilityCallback,
    ACameraManager_unregisterExtendedAvailabilityCallback, ACameraMetadata,
    ACameraMetadata_const_entry, ACameraMetadata_copy, ACameraMetadata_free,
    ACameraMetadata_getAllTags, ACameraMetadata_getConstEntry,
    ACameraMetadata_isLogicalMultiCamera, ACameraMetadata_rational, ACameraOutputTarget,
    ACameraOutputTarget_create, ACameraOutputTarget_free, ACameraWindowType, ACaptureRequest,
    ACaptureRequest_addTarget, ACaptureRequest_copy, ACaptureRequest_free,
    ACaptureRequest_getAllTags, ACaptureRequest_getConstEntry,
    ACaptureRequest_getConstEntry_physicalCamera, ACaptureRequest_getUserContext,
    ACaptureRequest_removeTarget, ACaptureRequest_setEntry_double, ACaptureRequest_setEntry_float,
    ACaptureRequest_setEntry_i32, ACaptureRequest_setEntry_i64,
    ACaptureRequest_setEntry_physicalCamera_double, ACaptureRequest_setEntry_physicalCamera_float,
    ACaptureRequest_setEntry_physicalCamera_i32, ACaptureRequest_setEntry_physicalCamera_i64,
    ACaptureRequest_setEntry_physicalCamera_rational, ACaptureRequest_setEntry_physicalCamera_u8,
    ACaptureRequest_setEntry_rational, ACaptureRequest_setEntry_u8, ACaptureRequest_setUserContext,
    ACaptureSessionOutput, ACaptureSessionOutputContainer, ACaptureSessionOutputContainer_add,
    ACaptureSessionOutputContainer_create, ACaptureSessionOutputContainer_free,
    ACaptureSessionOutputContainer_remove, ACaptureSessionOutput_create,
    ACaptureSessionOutput_free, AHardwareBuffer, AHardwareBuffer_Desc, AHardwareBuffer_Planes,
    AHardwareBuffer_acquire, AHardwareBuffer_allocate, AHardwareBuffer_describe,
    AHardwareBuffer_isSupported, AHardwareBuffer_lock, AHardwareBuffer_lockAndGetInfo,
    AHardwareBuffer_lockPlanes, AHardwareBuffer_recvHandleFromUnixSocket, AHardwareBuffer_release,
    AHardwareBuffer_sendHandleToUnixSocket, AHardwareBuffer_unlock, AImage, AImageCropRect,
    AImageReader, AImageReader_BufferRemovedListener, AImageReader_ImageListener,
    AImageReader_acquireLatestImage, AImageReader_acquireLatestImageAsync,
    AImageReader_acquireNextImage, AImageReader_acquireNextImageAsync, AImageReader_delete,
    AImageReader_getFormat, AImageReader_getHeight, AImageReader_getMaxImages,
    AImageReader_getWidth, AImageReader_getWindow, AImageReader_new, AImageReader_newWithUsage,
    AImageReader_setBufferRemovedListener, AImageReader_setImageListener, AImage_delete,
    AImage_deleteAsync, AImage_getCropRect, AImage_getFormat, AImage_getHardwareBuffer,
    AImage_getHeight, AImage_getNumberOfPlanes, AImage_getPlaneData, AImage_getPlanePixelStride,
    AImage_getPlaneRowStride, AImage_getTimestamp, AImage_getWidth, ANativeWindow,
    ANativeWindow_Buffer, ANativeWindow_acquire, ANativeWindow_fromSurface,
    ANativeWindow_getBuffersDataSpace, ANativeWindow_getFormat, ANativeWindow_getHeight,
    ANativeWindow_getWidth, ANativeWindow_lock, ANativeWindow_release,
    ANativeWindow_setBuffersDataSpace, ANativeWindow_setBuffersGeometry,
    ANativeWindow_setBuffersTransform, ANativeWindow_unlockAndPost, ARect, JNIEnv,
};
use core::slice;
use std::ffi::{CStr, CString};
use std::mem::MaybeUninit;
use std::os::raw::c_char;
use std::ptr::{drop_in_place, null_mut};
use std::sync::atomic::{AtomicPtr, Ordering};

impl ACameraMetadata_const_entry {
    pub(crate) fn get_i32(&self, offset: isize) -> i32 {
        unsafe { *((self.data as *const i32).offset(offset)) }
    }
    pub(crate) fn get_u8(&self, offset: isize) -> u8 {
        unsafe { *((self.data as *const u8).offset(offset)) }
    }
    pub(crate) fn get_i64(&self, offset: isize) -> i64 {
        unsafe { *((self.data as *const i64).offset(offset)) }
    }
    pub(crate) fn get_f(&self, offset: isize) -> f32 {
        unsafe { *((self.data as *const f32).offset(offset)) }
    }
    pub(crate) fn get_d(&self, offset: isize) -> f64 {
        unsafe { *((self.data as *const f64).offset(offset)) }
    }
    pub(crate) fn get_r(&self, offset: isize) -> ACameraMetadata_rational {
        unsafe { *((self.data as *const ACameraMetadata_rational).offset(offset)) }
    }
}
/*
trait AtomicStruct<T> {
    fn get_mut_ptr(&mut self) -> *mut T
    where
        Self: std::marker::Sized,
    {
        self.pointer.load(Ordering::SeqCst)
    }
}

impl AtomicStruct<ACameraDevice> for WACameraDevice {}
impl AtomicStruct<ANativeWindow> for WANativeWindow {}
impl AtomicStruct<ACameraManager> for WACameraManager {}
impl AtomicStruct<AImage> for WAImage {}
impl AtomicStruct<AHardwareBuffer> for WAHardwareBuffer {}
impl AtomicStruct<ACameraCaptureSession> for WACameraCaptureSession {}
impl AtomicStruct<ACaptureSessionOutput> for WACaptureSessionOutput {}
impl AtomicStruct<ACameraOutputTarget> for WACameraOutputTarget {}
impl AtomicStruct<ACaptureRequest> for WACaptureRequest {}
impl AtomicStruct<ACaptureSessionOutputContainer> for WACaptureSessionOutputContainer {}
impl AtomicStruct<AImageReader> for WAImageReader {}
*/
pub(crate) struct WACameraDevice {
    pointer: AtomicPtr<ACameraDevice>,
}
impl WACameraDevice {
    //Creates in WACameraManager::openCamera
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACameraDevice {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACameraDevice {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn close(&self) {
        unsafe_and_check_camera! { ACameraDevice_close(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getId(&self) -> std::os::raw::c_char {
        unsafe_no_check! { *ACameraDevice_getId(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn createCaptureRequest(
        &self,
        templateId: ACameraDevice_request_template,
    ) -> WACaptureRequest {
        let mut request_ = MaybeUninit::<*mut ACaptureRequest>::uninit();
        let ptr = self.pointer.load(Ordering::SeqCst);
        unsafe_and_check_camera! { ACameraDevice_createCaptureRequest(ptr, templateId, request_.as_mut_ptr()) };
        WACaptureRequest {
            pointer: AtomicPtr::new(unsafe_no_check! { request_.assume_init() }),
        }
    }

    pub fn createCaptureSession(
        &self,
        outputs: *const ACaptureSessionOutputContainer,
        callbacks: *const ACameraCaptureSession_stateCallbacks,
    ) -> WACameraCaptureSession {
        let mut captureSession = MaybeUninit::<*mut ACameraCaptureSession>::uninit();
        unsafe_and_check_camera! { ACameraDevice_createCaptureSession(self.pointer.load(Ordering::SeqCst), outputs, callbacks, captureSession.as_mut_ptr()) };
        let sess = unsafe_no_check! { captureSession.assume_init() };
        debug!("Created session {:?}", sess);
        WACameraCaptureSession {
            pointer: AtomicPtr::new(sess),
        }
    }

    pub fn createCaptureSessionWithSessionParameters(
        &self,
        outputs: *const ACaptureSessionOutputContainer,
        sessionParameters: *const ACaptureRequest,
        callbacks: *const ACameraCaptureSession_stateCallbacks,
    ) -> WACameraCaptureSession {
        let mut captureSession = MaybeUninit::<*mut ACameraCaptureSession>::uninit();
        unsafe_and_check_camera! { ACameraDevice_createCaptureSessionWithSessionParameters(self.pointer.load(Ordering::SeqCst), outputs, sessionParameters, callbacks, captureSession.as_mut_ptr()) };
        WACameraCaptureSession {
            pointer: AtomicPtr::new(unsafe_no_check! { captureSession.assume_init() }),
        }
    }

    pub fn createCaptureRequest_withPhysicalIds(
        &self,
        templateId: ACameraDevice_request_template,
        physicalIdList: *const ACameraIdList,
    ) -> AtomicPtr<ACaptureRequest> {
        let mut request_ = MaybeUninit::<*mut ACaptureRequest>::uninit();
        unsafe_and_check_camera! { ACameraDevice_createCaptureRequest_withPhysicalIds(self.pointer.load(Ordering::SeqCst), templateId, physicalIdList, request_.as_mut_ptr()) };
        AtomicPtr::new(unsafe_no_check! { request_.assume_init() })
    }

    pub fn isSessionConfigurationSupported(
        &self,
        sessionOutputContainer: *const ACaptureSessionOutputContainer,
    ) {
        unsafe_and_check_camera! { ACameraDevice_isSessionConfigurationSupported(self.pointer.load(Ordering::SeqCst), sessionOutputContainer) }
    }
}
impl Drop for WACameraDevice {
    fn drop(&mut self) {
        self.close();
    }
}

pub(crate) struct WACameraMetadata {
    pointer: AtomicPtr<ACameraMetadata>,
}
impl WACameraMetadata {
    //Creates in WACameraManager::getCameraCharacteristics
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACameraMetadata {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACameraMetadata {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn getConstEntry(&self, tag: acamera_metadata_tag) -> ACameraMetadata_const_entry {
        let mut _entry = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
        let ptr = self.pointer.load(Ordering::SeqCst);
        unsafe_and_check_camera! { ACameraMetadata_getConstEntry(ptr, tag as u32, _entry.as_mut_ptr()) };
        unsafe_no_check! { _entry.assume_init() }
    }

    pub fn getAllTags(&self) -> &[u32] {
        let mut numEntries = 0;
        let mut tags = MaybeUninit::<*const u32>::uninit();
        let ptr = self.pointer.load(Ordering::SeqCst);
        unsafe_and_check_camera! { ACameraMetadata_getAllTags(ptr, &mut numEntries, tags.as_mut_ptr()) };
        unsafe_no_check! { slice::from_raw_parts(tags.assume_init(), numEntries as usize) }
    }

    pub fn copy(&self) -> Self {
        Self {
            pointer: AtomicPtr::new(unsafe_no_check!(ACameraMetadata_copy(
                self.pointer.load(Ordering::SeqCst)
            ))),
        }
    }

    pub fn free(&self) {
        unsafe_no_check! { ACameraMetadata_free(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn isLogicalMultiCamera(
        &self,
        numPhysicalCameras: *mut usize,
        physicalCameraIds: *mut *const *const std::os::raw::c_char,
    ) -> bool {
        unsafe_no_check! { ACameraMetadata_isLogicalMultiCamera(self.pointer.load(Ordering::SeqCst), numPhysicalCameras, physicalCameraIds) }
    }
}
impl Drop for WACameraMetadata {
    fn drop(&mut self) {
        self.free();
    }
}

pub(crate) struct WACameraManager {
    pointer: AtomicPtr<ACameraManager>,
}
impl WACameraManager {
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACameraManager {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACameraManager {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn create() -> Self {
        Self {
            pointer: AtomicPtr::new(unsafe_no_check!(ACameraManager_create())),
        }
    }

    pub fn delete(&self) {
        unsafe_no_check! { ACameraManager_delete(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getCameraIdList(&self) -> Vec<CString> {
        let mut _id_list = MaybeUninit::<*mut ACameraIdList>::uninit();
        unsafe_and_check_camera! { ACameraManager_getCameraIdList(self.pointer.load(Ordering::SeqCst), _id_list.as_mut_ptr()) };
        let id_list = unsafe_no_check! { *_id_list.assume_init() };
        let cstrs: &[*const c_char] =
            unsafe { slice::from_raw_parts(id_list.cameraIds, id_list.numCameras as usize) };
        unsafe {
            cstrs
                .into_iter()
                .map(|p| CStr::from_ptr(*p).to_owned())
                .collect()
        }
    }

    pub fn registerAvailabilityCallback(
        &self,
        callback: *const ACameraManager_AvailabilityCallbacks,
    ) {
        unsafe_and_check_camera! { ACameraManager_registerAvailabilityCallback(self.pointer.load(Ordering::SeqCst), callback) }
    }

    pub fn unregisterAvailabilityCallback(
        &self,
        callback: *const ACameraManager_AvailabilityCallbacks,
    ) {
        unsafe_and_check_camera! { ACameraManager_unregisterAvailabilityCallback(self.pointer.load(Ordering::SeqCst), callback) }
    }

    pub fn getCameraCharacteristics(&self, cameraId: &CString) -> WACameraMetadata {
        let mut _metadata = MaybeUninit::<*mut ACameraMetadata>::uninit();
        unsafe_and_check_camera! { ACameraManager_getCameraCharacteristics(self.pointer.load(Ordering::SeqCst), cameraId.as_ptr(), _metadata.as_mut_ptr()) };
        WACameraMetadata {
            pointer: AtomicPtr::new(unsafe_no_check! { _metadata.assume_init() }),
        }
    }

    pub fn openCamera(
        &self,
        cameraId: *const std::os::raw::c_char,
        callback: *mut ACameraDevice_StateCallbacks,
    ) -> WACameraDevice {
        let mut _device = MaybeUninit::<*mut ACameraDevice>::uninit();
        unsafe_and_check_camera! { ACameraManager_openCamera(self.pointer.load(Ordering::SeqCst), cameraId, callback, _device.as_mut_ptr()) };
        WACameraDevice {
            pointer: AtomicPtr::new(unsafe_no_check! { _device.assume_init() }),
        }
    }

    pub fn registerExtendedAvailabilityCallback(
        &self,
        callback: *const ACameraManager_ExtendedAvailabilityCallbacks,
    ) {
        unsafe_and_check_camera! { ACameraManager_registerExtendedAvailabilityCallback(self.pointer.load(Ordering::SeqCst), callback) }
    }

    pub fn unregisterExtendedAvailabilityCallback(
        &self,
        callback: *const ACameraManager_ExtendedAvailabilityCallbacks,
    ) {
        unsafe_and_check_camera! { ACameraManager_unregisterExtendedAvailabilityCallback(self.pointer.load(Ordering::SeqCst), callback) }
    }
}
impl Drop for WACameraManager {
    fn drop(&mut self) {
        self.delete();
    }
}

pub(crate) struct WAHardwareBuffer {
    pointer: AtomicPtr<AHardwareBuffer>,
}
impl WAHardwareBuffer {
    //Creates in WAImage::getHardwareBuffer
    pub fn get_mut_ptr(&mut self) -> &mut *mut AHardwareBuffer {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut AHardwareBuffer {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn allocate(desc: *const AHardwareBuffer_Desc) -> Self {
        let mut _buffer = MaybeUninit::<*mut AHardwareBuffer>::uninit();
        unsafe_and_check_zero! { AHardwareBuffer_allocate(desc, _buffer.as_mut_ptr()) };
        Self {
            pointer: AtomicPtr::new(unsafe_no_check! { _buffer.assume_init() }),
        }
    }

    pub fn acquire(&self) {
        unsafe_no_check! { AHardwareBuffer_acquire(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn release(&self) {
        unsafe_no_check! { AHardwareBuffer_release(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn describe(&self, outDesc: *mut AHardwareBuffer_Desc) {
        unsafe_no_check! { AHardwareBuffer_describe(self.pointer.load(Ordering::SeqCst), outDesc) }
    }

    pub fn lock(
        &self,
        usage: u64,
        fence: i32,
        rect: *const ARect,
        outVirtualAddress: *mut *mut std::os::raw::c_void,
    ) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_lock(self.pointer.load(Ordering::SeqCst), usage, fence, rect, outVirtualAddress) }
    }

    pub fn lockPlanes(
        &self,
        usage: u64,
        fence: i32,
        rect: *const ARect,
        outPlanes: *mut AHardwareBuffer_Planes,
    ) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_lockPlanes(self.pointer.load(Ordering::SeqCst), usage, fence, rect, outPlanes) }
    }

    pub fn unlock(&self, fence: *mut i32) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_unlock(self.pointer.load(Ordering::SeqCst), fence) }
    }

    pub fn sendHandleToUnixSocket(&self, socketFd: std::os::raw::c_int) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_sendHandleToUnixSocket(self.pointer.load(Ordering::SeqCst), socketFd) }
    }

    pub fn recvHandleFromUnixSocket(&self, socketFd: std::os::raw::c_int) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_recvHandleFromUnixSocket(socketFd, &mut self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn isSupported(&self, desc: *const AHardwareBuffer_Desc) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_isSupported(desc) }
    }

    pub fn lockAndGetInfo(
        &self,
        usage: u64,
        fence: i32,
        rect: *const ARect,
        outVirtualAddress: *mut *mut std::os::raw::c_void,
        outBytesPerPixel: *mut i32,
        outBytesPerStride: *mut i32,
    ) -> std::os::raw::c_int {
        unsafe_no_check! { AHardwareBuffer_lockAndGetInfo(self.pointer.load(Ordering::SeqCst), usage, fence, rect, outVirtualAddress, outBytesPerPixel, outBytesPerStride) }
    }
}
impl Drop for WAHardwareBuffer {
    fn drop(&mut self) {
        self.release();
    }
}

pub(crate) struct WACameraOutputTarget {
    pointer: AtomicPtr<ACameraOutputTarget>,
}
impl WACameraOutputTarget {
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACameraOutputTarget {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACameraOutputTarget {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn create(anw: *mut ACameraWindowType) -> Self {
        let mut outputTarget = MaybeUninit::<*mut ACameraOutputTarget>::uninit();
        unsafe_and_check_camera!(ACameraOutputTarget_create(anw, outputTarget.as_mut_ptr()));
        Self {
            pointer: AtomicPtr::new(unsafe_no_check! { outputTarget.assume_init() }),
        }
    }
    pub fn free(&self) {
        unsafe_no_check! { ACameraOutputTarget_free(self.pointer.load(Ordering::SeqCst)) }
    }
}
impl Drop for WACameraOutputTarget {
    fn drop(&mut self) {
        self.free();
    }
}

pub(crate) struct WACaptureRequest {
    pointer: AtomicPtr<ACaptureRequest>,
}
impl WACaptureRequest {
    //Creates in WACameraDevice::createCaptureRequest
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACaptureRequest {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACaptureRequest {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn addTarget(&self, output: *const ACameraOutputTarget) {
        unsafe_and_check_camera! { ACaptureRequest_addTarget(self.pointer.load(Ordering::SeqCst), output) }
    }

    pub fn removeTarget(&self, output: *const ACameraOutputTarget) {
        unsafe_and_check_camera! { ACaptureRequest_removeTarget(self.pointer.load(Ordering::SeqCst), output) }
    }

    pub fn getConstEntry(&self, tag: u32) -> ACameraMetadata_const_entry {
        let mut _entry = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
        unsafe_and_check_camera! { ACaptureRequest_getConstEntry(self.pointer.load(Ordering::SeqCst), tag, _entry.as_mut_ptr()) };
        unsafe_no_check! { _entry.assume_init() }
    }

    pub fn getAllTags(&self) -> &[u32] {
        let mut numEntries = 0;
        let mut tags = MaybeUninit::<*const u32>::uninit();
        unsafe_and_check_camera! { ACaptureRequest_getAllTags(self.pointer.load(Ordering::SeqCst), &mut numEntries, tags.as_mut_ptr()) }
        unsafe_no_check! { slice::from_raw_parts(tags.assume_init(), numEntries as usize) }
    }

    pub fn setEntry_u8(&self, tag: acamera_metadata_tag, count: u32, data: u8) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_u8(self.pointer.load(Ordering::SeqCst), tag as u32, count, &data) }
    }

    pub fn setEntry_i32(&self, tag: acamera_metadata_tag, count: u32, data: i32) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_i32(self.pointer.load(Ordering::SeqCst), tag as u32, count, &data) }
    }

    pub fn setEntry_float(&self, tag: acamera_metadata_tag, count: u32, data: f32) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_float(self.pointer.load(Ordering::SeqCst), tag as u32, count, &data) }
    }

    pub fn setEntry_i64(&self, tag: acamera_metadata_tag, count: u32, data: i64) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_i64(self.pointer.load(Ordering::SeqCst), tag as u32, count, &data) }
    }

    pub fn setEntry_double(&self, tag: acamera_metadata_tag, count: u32, data: f64) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_double(self.pointer.load(Ordering::SeqCst), tag as u32, count, &data) }
    }

    pub fn setEntry_rational(&self, tag: u32, count: u32, data: *const ACameraMetadata_rational) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_rational(self.pointer.load(Ordering::SeqCst), tag, count, data) }
    }

    pub fn free(&self) {
        unsafe_no_check! { ACaptureRequest_free(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn setUserContext(&self, context: *mut std::os::raw::c_void) {
        unsafe_and_check_camera! { ACaptureRequest_setUserContext(self.pointer.load(Ordering::SeqCst), context) }
    }

    pub fn getUserContext(&self) -> AtomicPtr<std::os::raw::c_void> {
        let mut context = MaybeUninit::<*mut std::os::raw::c_void>::uninit();
        unsafe_and_check_camera! { ACaptureRequest_getUserContext(self.pointer.load(Ordering::SeqCst), context.as_mut_ptr()) };
        AtomicPtr::new(unsafe_no_check! { context.assume_init() })
    }

    pub fn copy(&self) -> Self {
        let ptr = self.pointer.load(Ordering::SeqCst);
        Self {
            pointer: AtomicPtr::new(unsafe_no_check!(ACaptureRequest_copy(ptr))),
        }
    }

    pub fn getConstEntry_physicalCamera(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
    ) -> ACameraMetadata_const_entry {
        let mut _entry = MaybeUninit::<ACameraMetadata_const_entry>::uninit();
        unsafe_and_check_camera! { ACaptureRequest_getConstEntry_physicalCamera(self.pointer.load(Ordering::SeqCst), physicalId, tag, _entry.as_mut_ptr()) };
        unsafe_no_check! { _entry.assume_init() }
    }

    pub fn setEntry_physicalCamera_u8(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const u8,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_u8(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }

    pub fn setEntry_physicalCamera_i32(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const i32,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_i32(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }

    pub fn setEntry_physicalCamera_float(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const f32,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_float(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }

    pub fn setEntry_physicalCamera_i64(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const i64,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_i64(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }

    pub fn setEntry_physicalCamera_double(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const f64,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_double(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }

    pub fn setEntry_physicalCamera_rational(
        &self,
        physicalId: *const std::os::raw::c_char,
        tag: u32,
        count: u32,
        data: *const ACameraMetadata_rational,
    ) {
        unsafe_and_check_camera! { ACaptureRequest_setEntry_physicalCamera_rational(self.pointer.load(Ordering::SeqCst), physicalId, tag, count, data) }
    }
}
impl Drop for WACaptureRequest {
    fn drop(&mut self) {
        self.free();
    }
}

pub(crate) struct WANativeWindow {
    pointer: AtomicPtr<ANativeWindow>,
}
impl WANativeWindow {
    pub fn get_mut_ptr(&mut self) -> &mut *mut ANativeWindow {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ANativeWindow {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn fromSurface(env: *mut JNIEnv, surface: jobject) -> Self {
        Self {
            pointer: AtomicPtr::new(unsafe_no_check!(ANativeWindow_fromSurface(env, surface))),
        }
    }
    pub fn acquire(&self) {
        let ptr = self.pointer.load(Ordering::SeqCst);
        unsafe_no_check! { ANativeWindow_acquire(ptr) };
    }

    pub fn release(&self) {
        unsafe_no_check! { ANativeWindow_release(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getWidth(&self) -> i32 {
        unsafe_no_check! { ANativeWindow_getWidth(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getHeight(&self) -> i32 {
        unsafe_no_check! { ANativeWindow_getHeight(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getFormat(&self) -> i32 {
        unsafe_no_check! { ANativeWindow_getFormat(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn setBuffersGeometry(&self, width: i32, height: i32, format: i32) -> i32 {
        unsafe_no_check! { ANativeWindow_setBuffersGeometry(self.pointer.load(Ordering::SeqCst), width, height, format) }
    }

    pub fn lock(&self, outBuffer: *mut ANativeWindow_Buffer, inOutDirtyBounds: *mut ARect) -> i32 {
        unsafe_no_check! { ANativeWindow_lock(self.pointer.load(Ordering::SeqCst), outBuffer, inOutDirtyBounds) }
    }

    pub fn unlockAndPost(&self) -> i32 {
        unsafe_no_check! { ANativeWindow_unlockAndPost(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn setBuffersTransform(&self, transform: i32) -> i32 {
        unsafe_no_check! { ANativeWindow_setBuffersTransform(self.pointer.load(Ordering::SeqCst), transform) }
    }

    pub fn setBuffersDataSpace(&self, dataSpace: i32) -> i32 {
        unsafe_no_check! { ANativeWindow_setBuffersDataSpace(self.pointer.load(Ordering::SeqCst), dataSpace) }
    }

    pub fn getBuffersDataSpace(&self) -> i32 {
        unsafe_no_check! { ANativeWindow_getBuffersDataSpace(self.pointer.load(Ordering::SeqCst)) }
    }
}
impl Drop for WANativeWindow {
    fn drop(&mut self) {
        self.release();
    }
}

pub(crate) struct WACaptureSessionOutputContainer {
    pointer: AtomicPtr<ACaptureSessionOutputContainer>,
}
impl WACaptureSessionOutputContainer {
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACaptureSessionOutputContainer {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACaptureSessionOutputContainer {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn create() -> Self {
        let mut container = MaybeUninit::<*mut ACaptureSessionOutputContainer>::uninit();
        unsafe_and_check_camera! {ACaptureSessionOutputContainer_create(
            container.as_mut_ptr()
        )};
        Self {
            pointer: unsafe { AtomicPtr::new(container.assume_init()) },
        }
    }

    pub fn free(&self) {
        unsafe_no_check! { ACaptureSessionOutputContainer_free(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn add(&self, output: *const ACaptureSessionOutput) {
        unsafe_and_check_camera! { ACaptureSessionOutputContainer_add(self.pointer.load(Ordering::SeqCst), output) }
    }

    pub fn remove(&self, output: *const ACaptureSessionOutput) {
        unsafe_and_check_camera! { ACaptureSessionOutputContainer_remove(self.pointer.load(Ordering::SeqCst), output) }
    }
}
impl Drop for WACaptureSessionOutputContainer {
    fn drop(&mut self) {
        self.free();
    }
}

pub(crate) struct WAImage {
    pointer: AtomicPtr<AImage>,
}
impl WAImage {
    //Creates in WAImageReader::acquireNext/LastImage
    pub fn get_mut_ptr(&mut self) -> &mut *mut AImage {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut AImage {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn delete(&self) {
        unsafe_no_check! { AImage_delete(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getWidth(&self) -> i32 {
        let mut width = 0;
        unsafe_and_check_media! { AImage_getWidth(self.pointer.load(Ordering::SeqCst), &mut width) };
        width
    }

    pub fn getHeight(&self) -> i32 {
        let mut height = 0;
        unsafe_and_check_media! { AImage_getHeight(self.pointer.load(Ordering::SeqCst), &mut height) };
        height
    }

    pub fn getFormat(&self) -> i32 {
        let mut format = 0;
        unsafe_and_check_media! { AImage_getFormat(self.pointer.load(Ordering::SeqCst), &mut format) };
        format
    }

    pub fn getCropRect(&self) -> AImageCropRect {
        let mut _rect = MaybeUninit::<AImageCropRect>::uninit();
        unsafe_and_check_media! { AImage_getCropRect(self.pointer.load(Ordering::SeqCst), _rect.as_mut_ptr()) };
        unsafe_no_check! { _rect.assume_init() }
    }

    pub fn getTimestamp(&self) -> i64 {
        let mut timestampNs = 0;
        unsafe_and_check_media! { AImage_getTimestamp(self.pointer.load(Ordering::SeqCst), &mut timestampNs) };
        timestampNs
    }

    pub fn getNumberOfPlanes(&self) -> i32 {
        let mut numPlanes = 0;
        unsafe_and_check_media! { AImage_getNumberOfPlanes(self.pointer.load(Ordering::SeqCst), &mut numPlanes) };
        numPlanes
    }

    pub fn getPlanePixelStride(&self, planeIdx: std::os::raw::c_int) -> i32 {
        let mut pixelStride = 0;
        unsafe_and_check_media! { AImage_getPlanePixelStride(self.pointer.load(Ordering::SeqCst), planeIdx, &mut pixelStride) };
        pixelStride
    }

    pub fn getPlaneRowStride(&self, planeIdx: std::os::raw::c_int) -> i32 {
        let mut rowStride = 0;
        unsafe_and_check_media! { AImage_getPlaneRowStride(self.pointer.load(Ordering::SeqCst), planeIdx, &mut rowStride) };
        rowStride
    }

    pub fn getPlaneData(
        &self,
        planeIdx: std::os::raw::c_int,
        data: *mut *mut u8,
        dataLength: *mut std::os::raw::c_int,
    ) -> &[u8] {
        let mut _data = MaybeUninit::<*mut u8>::uninit();
        let mut len = 0;
        unsafe_and_check_media! { AImage_getPlaneData(self.pointer.load(Ordering::SeqCst), planeIdx, _data.as_mut_ptr(), &mut len) };
        let data = unsafe_no_check! { slice::from_raw_parts(_data.assume_init(), len as usize) };
        data
    }

    pub fn deleteAsync(&self, releaseFenceFd: std::os::raw::c_int) {
        unsafe_no_check! { AImage_deleteAsync(self.pointer.load(Ordering::SeqCst), releaseFenceFd) }
    }

    pub fn getHardwareBuffer(&self, buffer: *mut *mut AHardwareBuffer) -> WAHardwareBuffer {
        let mut _buffer = MaybeUninit::<*mut AHardwareBuffer>::uninit();
        unsafe_and_check_media! { AImage_getHardwareBuffer(self.pointer.load(Ordering::SeqCst), _buffer.as_mut_ptr()) };
        WAHardwareBuffer {
            pointer: AtomicPtr::new(unsafe_no_check! {_buffer.assume_init()}),
        }
    }
}
impl Drop for WAImage {
    fn drop(&mut self) {
        self.delete();
    }
}

pub(crate) struct WACameraCaptureSession {
    pointer: AtomicPtr<ACameraCaptureSession>,
}
impl WACameraCaptureSession {
    //Creates in WACameraDevice::createCaptureSession
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACameraCaptureSession {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACameraCaptureSession {
        self.pointer.load(Ordering::Relaxed)
    }

    pub fn close(&self) {
        unsafe_no_check! { ACameraCaptureSession_close(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getDevice(&self) -> WACameraDevice {
        let mut _device = MaybeUninit::<*mut ACameraDevice>::uninit();
        unsafe_and_check_camera! { ACameraCaptureSession_getDevice(self.pointer.load(Ordering::SeqCst), _device.as_mut_ptr()) };
        WACameraDevice {
            pointer: AtomicPtr::new(unsafe_no_check! {_device.assume_init()}),
        }
    }

    pub fn capture(
        &self,
        callbacks: *mut ACameraCaptureSession_captureCallbacks,
        numRequests: std::os::raw::c_int,
        requests: *mut *mut ACaptureRequest,
    ) -> i32 {
        let mut captureSequenceId = 0i32;
        unsafe_and_check_camera! { ACameraCaptureSession_capture(self.pointer.load(Ordering::SeqCst), callbacks, numRequests, requests, &mut captureSequenceId) };
        captureSequenceId
    }

    pub fn setRepeatingRequest(
        &self,
        callbacks: *mut ACameraCaptureSession_captureCallbacks,
        numRequests: std::os::raw::c_int,
        requests: *mut *mut ACaptureRequest,
        captureSequenceId: *mut std::os::raw::c_int,
    ) {
        unsafe_and_check_camera! { ACameraCaptureSession_setRepeatingRequest(self.pointer.load(Ordering::SeqCst), callbacks, numRequests, requests, captureSequenceId) }
    }

    pub fn stopRepeating(&self) {
        unsafe_and_check_camera! { ACameraCaptureSession_stopRepeating(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn abortCaptures(&self) {
        unsafe_and_check_camera! { ACameraCaptureSession_abortCaptures(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn updateSharedOutput(&self, output: *mut ACaptureSessionOutput) {
        unsafe_and_check_camera! { ACameraCaptureSession_updateSharedOutput(self.pointer.load(Ordering::SeqCst), output) }
    }

    pub fn logicalCamera_capture(
        &self,
        callbacks: *mut ACameraCaptureSession_logicalCamera_captureCallbacks,
        numRequests: std::os::raw::c_int,
        requests: *mut *mut ACaptureRequest,
        captureSequenceId: *mut std::os::raw::c_int,
    ) {
        unsafe_and_check_camera! { ACameraCaptureSession_logicalCamera_capture(self.pointer.load(Ordering::SeqCst), callbacks, numRequests, requests, captureSequenceId) }
    }

    pub fn logicalCamera_setRepeatingRequest(
        &self,
        callbacks: *mut ACameraCaptureSession_logicalCamera_captureCallbacks,
        numRequests: std::os::raw::c_int,
        requests: *mut *mut ACaptureRequest,
        captureSequenceId: *mut std::os::raw::c_int,
    ) {
        unsafe_and_check_camera! { ACameraCaptureSession_logicalCamera_setRepeatingRequest(self.pointer.load(Ordering::SeqCst), callbacks, numRequests, requests, captureSequenceId) }
    }
}
impl Drop for WACameraCaptureSession {
    fn drop(&mut self) {
        self.close();
    }
}

pub(crate) struct WACaptureSessionOutput {
    pointer: AtomicPtr<ACaptureSessionOutput>,
}
impl WACaptureSessionOutput {
    pub fn get_mut_ptr(&mut self) -> &mut *mut ACaptureSessionOutput {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut ACaptureSessionOutput {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn create(anw: *mut ACameraWindowType) -> Self {
        let mut sessionOutput = MaybeUninit::<*mut ACaptureSessionOutput>::uninit();
        unsafe_and_check_camera!(ACaptureSessionOutput_create(
            anw,
            sessionOutput.as_mut_ptr()
        ));
        Self {
            pointer: AtomicPtr::new(unsafe_no_check! { sessionOutput.assume_init() }),
        }
    }
    pub fn free(&self) {
        unsafe_no_check! { ACaptureSessionOutput_free(self.pointer.load(Ordering::SeqCst)) }
    }
}
impl Drop for WACaptureSessionOutput {
    fn drop(&mut self) {
        self.free();
    }
}

pub(crate) struct WAImageReader {
    pointer: AtomicPtr<AImageReader>,
}
impl WAImageReader {
    pub fn get_mut_ptr(&mut self) -> &mut *mut AImageReader {
        self.pointer.get_mut()
    }

    pub fn load(&self) -> *mut AImageReader {
        self.pointer.load(Ordering::SeqCst)
    }

    pub fn new(width: i32, height: i32, format: i32, maxImages: i32) -> Self {
        let mut _reader = MaybeUninit::<*mut AImageReader>::uninit();
        unsafe_and_check_media! { AImageReader_new(width, height, format, maxImages, _reader.as_mut_ptr()) };
        Self {
            pointer: AtomicPtr::new(unsafe_no_check! { _reader.assume_init() }),
        }
    }
    pub fn newWithUsage(width: i32, height: i32, format: i32, usage: u64, maxImages: i32) -> Self {
        let mut _reader = MaybeUninit::<*mut AImageReader>::uninit();
        unsafe_and_check_media! { AImageReader_newWithUsage(width, height, format, usage, maxImages, _reader.as_mut_ptr()) }
        Self {
            pointer: AtomicPtr::new(unsafe_no_check! { _reader.assume_init() }),
        }
    }

    pub fn delete(&self) {
        unsafe_no_check! { AImageReader_delete(self.pointer.load(Ordering::SeqCst)) }
    }

    pub fn getWindow(&self) -> WANativeWindow {
        let mut _window = MaybeUninit::<*mut ANativeWindow>::uninit();
        unsafe_and_check_media! { AImageReader_getWindow(self.pointer.load(Ordering::SeqCst), _window.as_mut_ptr()) };
        WANativeWindow {
            pointer: AtomicPtr::new(unsafe_no_check! { _window.assume_init() }),
        }
    }

    pub fn getWidth(&self) -> i32 {
        let mut width: i32 = 0;
        unsafe_and_check_media! { AImageReader_getWidth(self.pointer.load(Ordering::SeqCst), &mut width) };
        width
    }

    pub fn getHeight(&self) -> i32 {
        let mut height: i32 = 0;
        unsafe_and_check_media! { AImageReader_getHeight(self.pointer.load(Ordering::SeqCst), &mut height) };
        height
    }

    pub fn getFormat(&self) -> i32 {
        let mut format: i32 = 0;
        unsafe_and_check_media! { AImageReader_getFormat(self.pointer.load(Ordering::SeqCst), &mut format) };
        format
    }

    pub fn getMaxImages(&self) -> i32 {
        let mut maxImages: i32 = 0;
        unsafe_and_check_media! { AImageReader_getMaxImages(self.pointer.load(Ordering::SeqCst), &mut maxImages) };
        maxImages
    }

    pub fn acquireNextImage(&self) -> Option<WAImage> {
        let mut _image = MaybeUninit::<*mut AImage>::uninit();
        let status = unsafe_no_check! { AImageReader_acquireNextImage(self.pointer.load(Ordering::SeqCst), _image.as_mut_ptr()) };
        match status {
            st if st == AMEDIA_OK => Some(WAImage {
                pointer: AtomicPtr::new(unsafe_no_check! { _image.assume_init() }),
            }),
            _ => None,
        }
    }

    pub fn acquireLatestImage(&self) -> Option<WAImage> {
        let mut _image = MaybeUninit::<*mut AImage>::uninit();
        let status = unsafe_no_check! { AImageReader_acquireLatestImage(self.pointer.load(Ordering::SeqCst), _image.as_mut_ptr()) };
        match status {
            st if st == AMEDIA_OK => Some(WAImage {
                pointer: AtomicPtr::new(unsafe_no_check! { _image.assume_init() }),
            }),
            _ => None,
        }
    }

    pub fn setImageListener(&self, listener: *mut AImageReader_ImageListener) {
        unsafe_and_check_media! { AImageReader_setImageListener(self.pointer.load(Ordering::SeqCst), listener) }
    }

    pub fn acquireNextImageAsync(
        &self,
        image: *mut *mut AImage,
        acquireFenceFd: *mut std::os::raw::c_int,
    ) {
        unsafe_and_check_media! { AImageReader_acquireNextImageAsync(self.pointer.load(Ordering::SeqCst), image, acquireFenceFd) }
    }

    pub fn acquireLatestImageAsync(
        &self,
        image: *mut *mut AImage,
        acquireFenceFd: *mut std::os::raw::c_int,
    ) {
        unsafe_and_check_media! { AImageReader_acquireLatestImageAsync(self.pointer.load(Ordering::SeqCst), image, acquireFenceFd) }
    }

    pub fn setBufferRemovedListener(&self, listener: *mut AImageReader_BufferRemovedListener) {
        unsafe_and_check_media! { AImageReader_setBufferRemovedListener(self.pointer.load(Ordering::SeqCst), listener) }
    }
}
impl Drop for WAImageReader {
    fn drop(&mut self) {
        self.delete();
    }
}
