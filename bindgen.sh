INC="/media/Compressed/Drivers_bios/src/dev/Android/ndk/toolchains/allfresh/sysroot/usr/include/"

FILES="camera/NdkCameraManager camera/NdkCameraError camera/NdkCameraDevice camera/NdkCameraMetadataTags android/native_window_jni media/NdkImage media/NdkImageReader vulkan/vulkan_core vulkan/vk_platform vulkan/vulkan_android vulkan/vulkan"
OUT="/tmp/header"
HF="${OUT}.h"

rm $HF

for f in $FILES; do
    echo "#include \"${f}.h\"" >> $HF
done

echo "#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(dead_code)]" > "${OUT}.rs";
bindgen --rustified-enum "*" --use-array-pointers-in-arguments --use-core --builtins $HF -- -I$INC -include stdint.h -include stdbool.h >> "${OUT}.rs";
sed -i -e 's:Eq, Hash):Eq, Hash, FromPrimitive):g' ${OUT}.rs
