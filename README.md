1. Setup path to linker in **.cargo/config**

```
[target.aarch64-linux-android]
linker = "/path/to/ndk64/bin/aarch64-linux-android-gcc"

[target.arm-linux-androideabi]
linker = "/path/to/ndk/bin/arm-linux-androideabi-gcc"
```


2. Compile project with **./gradlew a**

App don't ask storage permission, so you have to grant it manually in settings if you want to save pictures.